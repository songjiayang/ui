import React from 'react'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'
import { observer, inject } from 'mobx-react'

const styles = (theme) => ({
  main: {
    marginBottom: theme.spacing.unit * 1,
    marginTop: theme.spacing.unit * 0.5,
  }
})

@withStyles(styles)
@inject('SettingsStore')
@observer
export default class CardHeader extends React.Component {
  render (){
    const { classes, SettingsStore, children } = this.props

    if (!SettingsStore.settings.enableQuickInfoCards) {
      return <React.Fragment />
    }

    return (
      <Grid
        className={classes.main} container
        direction="row" alignItems="center"
        justify="space-between" spacing={8}
      >
        { React.Children.map(children, (el) =>
          <Grid item>
            {el}
          </Grid>
        ) }
      </Grid>
    )
  }
}