import { action, observable } from "mobx"
import {
  addKnownPeer, getKnownPeers, getPeersConnected,
  getStatsBandiwdth, removeKnownPeer,
} from "../worker/connectivity"

class NetworkingStore {
  @observable public loading = true
  @observable public peers = []
  @observable public bandwidthUsage = {
    rateIn: {},
    rateOut: {},
  }
  // Used for pinging in Network Ping Dialog and menus
  @observable public isPingDialogOpen = false
  @observable public pingDialogAddress = ""

  @observable public knownPeers: any[] = []
  private fetching = false
  private interval = null

  @action
  public fetch = () => {
    if (this.fetching) {
      return Promise.resolve()
    }

    this.fetching = true
    return Promise.all([
      this.fetchPeers(),
      this.fetchBandiwdthUsage(),
    ]).then(() => {
      this.loading = false
      this.fetching = false
    })
  }

  @action
  public fetchKnownPeers = () => {
    return getKnownPeers()
      .then((kpeers) => {
        this.knownPeers = kpeers
      })
  }

  @action
  public fetchPeers = () => {
    return getPeersConnected()
      .then((peers) => {
        this.peers = peers
        this.knownPeers = this.knownPeers.map( (kpeer) => {
          kpeer.status = this.isPeerOnline(kpeer.id) ? "Online" : "Offline"
          return kpeer
        })
      })
  }

  @action
  public fetchBandiwdthUsage = () => {
    return getStatsBandiwdth()
      .then((stat) => {
        this.bandwidthUsage = stat
      })
  }

  @action
  public startLoop = (frequency = 2500) => {
    if (this.interval) {
      clearInterval(this.interval)
    }

    this.interval = setInterval(() => {
      if (this.fetching) {
        return
      }

      this.fetch()
      .catch((err) => { console.warn("[Networking Store] Error:", err) })
    }, frequency)
  }

  // Check if a peerId is present in the list of the peers
  public isPeerOnline = (peerId) => {
    return this.peers.some( (p) => p.id === peerId )
  }

  @action
  public stopLoop = () => {
    clearInterval(this.interval)
  }

  public addKnownPeer = (name: string, multiaddress: string): Promise<any> => {
    const components = multiaddress.split("/")
    const kPeerId = components[components.length - 1]

    if (!kPeerId) {
      return
    }

    // If the user already knows the multiaddress of the new known peer
    if (this.knownPeers.some((peer) => peer.address === multiaddress)) {
      return
    }

    return addKnownPeer({
        addresses: [multiaddress],
        id: kPeerId,
        name,
      })
      .then(() => this.fetchKnownPeers())
  }

  public removeKnownPeer = (id: string): Promise<any> => {
    return removeKnownPeer(id)
      .then(() => this.fetchKnownPeers())
  }
}

const store = new NetworkingStore()
export default store
