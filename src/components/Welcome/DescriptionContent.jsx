import React from "react"
import { withStyles } from "@material-ui/core/styles"
import DialogContentText from "@material-ui/core/DialogContentText"
import DialogContent from "@material-ui/core/DialogContent"
import OrionLogo from "../../assets/orion-logo.svg"
import SafeLinkButton from "../SafeLinkButton"

const SUBSCRIBE_LINK = "http://eepurl.com/dfB6q5"

class DescriptionContent extends React.Component {
  render () {
    const { classes } = this.props

    return (
      <DialogContent className={classes.content}>
        <img src={OrionLogo} alt="Orion logo" height="125px" />
        <DialogContentText>
          <h2>Welcome</h2>
          <p className={classes.text}>
            Orion is the easiest way to start using the decentralised
            web with IPFS and Siderus Network. It supports a larger number of
            dApps as well as the IPFS Browser Companion, to speed up your
            connection when surfing the decentralised web.
          </p>
          <p>This wizard will help you with the initial requirements.</p>
          <SafeLinkButton
            color="primary"
            variant="contained"
            size="large"
            className={classes.subscribeButton}
            href={SUBSCRIBE_LINK}
          >
            Subscribe to Siderus Newsletter
          </SafeLinkButton>
        </DialogContentText>
      </DialogContent>
    )
  }
}

const styles = {
  content: {
    textAlign: "center",
  },
  text: {
    textAlign: "left",
  },
  subscribeButton :{
    color: 'white',
  },
}

export default withStyles(styles)(DescriptionContent)
