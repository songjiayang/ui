import React from 'react'
import { withRouter } from 'react-router'
import { observer, inject } from 'mobx-react'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import TextField from '@material-ui/core/TextField'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import ConnectIcon from '@material-ui/icons/Phone'
import mafmt from 'mafmt'
//
import { connectTo, addBootstrapAddr } from '../../worker/connectivity'

const OPEN_CONNECT_DIALOG_BUTTON_LABEL = "Connect"
const CONNECT_PLACEHOLDER_TEXT = "/ip4/127.0.0.1/ipfs/Qm12342..."

const styles = theme => ({
  myAddressText: {
    overflow: 'hidden',
    marginRight: 20,
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  content: {
    minWidth: 500,
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  }
})

@withRouter
@withStyles(styles)
@inject('NetworkStore')
@observer
class AddressDialog extends React.Component {
  state = {
    open: false,
    isValid: true,
    isNameValid: true,
    nodeAddress: '',
    addToBootstrap: true,
    name: '',
    error: null,
  }

  handleOpen = () => {
    this.setState({
      open: true, isValid: true,
      isNameValid: true, nodeAddress: '',
      name: '', error: null,
    })
  }

  handleClose = () => {
    this.setState({ open: false, nodeAddress: '', name: '' })
  }


  handleSubmit = () => {
    const { nodeAddress, addToBootstrap, name, } = this.state
    const { NetworkStore } = this.props

    let isValid = false
    try {
      isValid = mafmt.IPFS.matches(nodeAddress)
    } catch (err) {
      isValid = false
    }

    if (!isValid) {
      return this.setState({ isValid: false })
    }

    const isNameValid = name.length >= 3
    if (!isNameValid) {
      return this.setState({ isNameValid })
    }

    Promise.all([
      NetworkStore.addKnownPeer(name, nodeAddress),
      connectTo(nodeAddress),
      addToBootstrap ? addBootstrapAddr(nodeAddress) : false,
    ])
      .then(() => {
        this.handleClose()
      })
      .catch((err) => {
        console.warn(err)
        this.setState({ error: err.toString() })
      })
  }

  render () {
    const { classes } = this.props
    const {
      open, isValid, error, nodeAddress,
      addToBootstrap, name, isNameValid
    } = this.state

    return (
      <React.Fragment>
        <Button onClick={this.handleOpen}>
          <ConnectIcon className={classes.leftIcon} />
            {OPEN_CONNECT_DIALOG_BUTTON_LABEL}
        </Button>
        <Dialog
          open={open}
          onClose={this.handleClose}
        >
          <DialogTitle>Connect to a peer</DialogTitle>
          <DialogContent className={classes.content}>
            <DialogContentText>
              Use this form to connect directly to a peer. Insert a reachable
              multiaddress and click "Connect". Orion will add it to the list of
              known peers, to easily remember it. You can also add a name to
              easily recognise it.
            </DialogContentText>
            <List>
              <ListItem>
                <ListItemText>
                  <TextField
                    fullWidth
                    label='Peer Name'
                    placeholder='Lumpy Space Princess Node'
                    value={name}
                    onChange={e => this.setState({ name: e.target.value })}
                    error={!isNameValid}
                    margin='none'
                  />
                </ListItemText>
              </ListItem>
              <ListItem>
                <ListItemText>
                  <TextField
                    fullWidth
                    label='Peer Multiaddress'
                    placeholder={CONNECT_PLACEHOLDER_TEXT}
                    value={nodeAddress}
                    onChange={e => this.setState({ nodeAddress: e.target.value })}
                    error={!isValid}
                    margin='none'
                  />
                </ListItemText>
              </ListItem>
              <ListItem>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={addToBootstrap}
                      onChange={() => { this.setState({ addToBootstrap: !addToBootstrap }) }}
                    />
                  }
                  label='Connect automatically when starting IPFS'
                />
              </ListItem>
            </List>
            {/* In case of error show a message */}
            { error &&
            <DialogContentText>
              An error occurred:<br />
              <br />
              { error }
              <br />
            </DialogContentText>
            }
          </DialogContent>
          <DialogActions>
          <Button onClick={this.handleClose}>
              Cancel
            </Button>
            <Button onClick={this.handleSubmit}>
              Connect
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    )
  }
}


export default AddressDialog