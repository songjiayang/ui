import React from 'react'
import { inject } from 'mobx-react'
import { withSnackbar } from 'notistack'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemText from '@material-ui/core/ListItemText'
import IconButton from '@material-ui/core/IconButton'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import Menu from '@material-ui/core/Menu'
import { withStyles } from '@material-ui/core/styles'
import Divider from '@material-ui/core/Divider'
import DeleteIcon from '@material-ui/icons/Delete'
import ConnectIcon from '@material-ui/icons/Phone'
import NetworkCheckIcon from '@material-ui/icons/NetworkCheck'
//
import {
  COMPLETED_SETTINGS,
  FAILED_SETTINGS,
} from '../../notifications'
import CopyButton from '../MenuElements/Copy'
import { connectTo } from '../../worker/connectivity'


const styles = {}

@withSnackbar
@withStyles(styles)
@inject('NetworkStore')
export default class PeerElement extends React.Component {
  state = {
    actionMenuAnchor: null
  }

  handleActionMenuOpen = (event) => {
    this.setState({ actionMenuAnchor: event.currentTarget })
  }

  handleActionMenuClose = () => {
    this.setState({ actionMenuAnchor: null })
  }

  handleRemove = () => {
    const { NetworkStore, peer } = this.props

    this.handleActionMenuClose()
    return NetworkStore.removeKnownPeer(peer.id)
      .then(() => {
        this.props.enqueueSnackbar(`Peer ${peer.name}, Removed`, COMPLETED_SETTINGS)
      })
      .catch((err) => {
        this.props.enqueueSnackbar(`Error Removing ${peer.name}: ${err}`, FAILED_SETTINGS)
      })
  }

  handlePing = () => {
    const { NetworkStore, peer } = this.props
    NetworkStore.pingDialogAddress = peer.addresses[0]
    NetworkStore.isPingDialogOpen = true
    this.handleActionMenuClose()
  }

  handleConnect = () => {
    const { peer } = this.props
    this.handleActionMenuClose()

    return Promise.race(peer.addresses.map((addr) => connectTo(addr)))
      .then(() => {
        this.props.enqueueSnackbar(`Peer "${peer.name}" connected`, COMPLETED_SETTINGS)
      })
      .catch((err) => {
        console.warn("Unable to connect to peer", peer, "Error:", err)
      })
  }

  render () {
    const { actionMenuAnchor } = this.state
    const { classes, peer, id } = this.props

    const ClickableCell = props => (
      <TableCell
        className={classes.clickableCell}
        {...props}
      />
    )

    let peerAddress = "Unknown"
    if (peer.addresses.length > 0) {
      peerAddress = peer.addresses[0]
    }

    return (
      <TableRow
        hover
        role='checkbox'
        forkeyid={id}
        selected={peer.status === 'Offline'}
      >
        <ClickableCell component='th' scope='row'>
          {peer.name || ''}
        </ClickableCell>
        <ClickableCell>
          {peerAddress || peer.id}
        </ClickableCell>
        <ClickableCell align="right">
          {peer.status || 'Loading...'}
        </ClickableCell>
        <TableCell align='right'>
          <IconButton
            aria-label='Actions'
            aria-owns={actionMenuAnchor ? 'action-menu' : null}
            aria-haspopup='true'
            onClick={this.handleActionMenuOpen}
          >
            <MoreHorizIcon />
          </IconButton>
          <Menu
            id='action-menu'
            anchorEl={actionMenuAnchor}
            open={Boolean(actionMenuAnchor)}
            onClose={this.handleActionMenuClose}
          >
            { peer.status === "Offline" &&
              <MenuItem key={0}>
                <ListItemIcon>
                  <ConnectIcon />
                </ListItemIcon>
                <ListItemText
                  inset
                  primary="Connect"
                  onClick={this.handleConnect}
                />
              </MenuItem>
            }
            { peer.status === "Offline" && <Divider key={1} /> }
            <MenuItem key={2}>
              <ListItemIcon>
                <NetworkCheckIcon />
              </ListItemIcon>
              <ListItemText
                inset
                primary="Ping"
                onClick={this.handlePing}
              />
            </MenuItem>
            <Divider key={3} />
            <CopyButton key={4} value={peerAddress} label="Copy Multiaddress" />
            <CopyButton key={5} value={peer.id} label="Copy Peer ID" />
            <Divider key={6} />
            <MenuItem key={7}>
              <ListItemIcon>
                <DeleteIcon />
              </ListItemIcon>
              <ListItemText
                inset
                primary="Remove"
                onClick={this.handleRemove}
              />
            </MenuItem>
          </Menu>
        </TableCell>
      </TableRow>
    )
  }

}