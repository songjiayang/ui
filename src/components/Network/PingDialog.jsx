import React from 'react'
import { withRouter } from 'react-router'
import { observer, inject } from 'mobx-react'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import CircularProgress from '@material-ui/core/CircularProgress'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import TextField from '@material-ui/core/TextField'
import NetworkCheckIcon from '@material-ui/icons/NetworkCheck'
import Typography from '@material-ui/core/Typography'
import mafmt from 'mafmt'

import { LineChart, Line, YAxis, XAxis, Tooltip } from 'recharts'
import { pingPeer } from '../../worker/connectivity'

const OPEN_PING_DIALOG_BUTTON_LABEL = "Ping"
const CONNECT_PLACEHOLDER_TEXT = "/ip4/127.0.0.1/ipfs/Qm12342..."

const styles = theme => ({
  resultsText: {
    paddingBottom: theme.spacing.unit * 2,
  },
  content: {
    minWidth: 500,
  },
  centered: {
    textAlign: 'center',
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  }
})

@withRouter
@withStyles(styles)
@inject('NetworkStore')
@observer
class PingDialog extends React.Component {
  state = {
    loading: false,
    isValid: true,
    results: [],
    error: '',
  }

  handleOpen = () => {
    this.setState({
      results: [], loading: false, isValid: true, error: '',
    })
    this.props.NetworkStore.pingDialogAddress = ''
    this.props.NetworkStore.isPingDialogOpen = true
  }

  handleClose = () => {
    this.setState({error: '', results: []})
    this.props.NetworkStore.pingDialogAddress = ''
    this.props.NetworkStore.isPingDialogOpen = false
  }

  startPing = () => {
    const address = this.props.NetworkStore.pingDialogAddress

    let isValid = false
    try {
      isValid = mafmt.IPFS.matches(address)
    } catch (err) {
      isValid = false
    }

    if(!isValid) {
      return this.setState({isValid})
    }

    // Set loading state
    this.setState({loading: true, results: []})
    return pingPeer(address, 10).then((data) => {
      const results = data.map((el) => {
        if (!el.time) return el
        const newTime = (el.time / 1000000).toFixed(2)
        el.time = newTime
        return el
      })
      this.setState({results, loading: false})
    }).catch((error) => {
      this.setState({results: [], loading: false, error})
    })

  }

  render () {
    const { classes, NetworkStore } = this.props
    const { loading, isValid, results, error } = this.state

    return (
      <React.Fragment>
        <Button onClick={this.handleOpen}>
          <NetworkCheckIcon className={classes.leftIcon} />
            {OPEN_PING_DIALOG_BUTTON_LABEL}
        </Button>
        <Dialog
          open={NetworkStore.isPingDialogOpen}
          onClose={this.handleClose}
          disableEscapeKeyDown={!!loading}
          disableBackdropClick={!!loading}
        >
          { results.length === 0 && !loading && !error && <DialogTitle>Ping an address</DialogTitle> }
          { results.length !== 0 && !loading && !error && <DialogTitle>Ping successful</DialogTitle> }
          { !loading && error && <DialogTitle>Ping failed</DialogTitle> }
          <DialogContent className={classes.content}>
          { /* When not loading and no results */}
          { results.length === 0 && !loading && !error &&
            <React.Fragment>
              <DialogContentText>
                Use this form to check if a peer with a specific ID / address is
                reachable or not. The process might take up to 60 seconds.
              </DialogContentText>
              <List>
                <ListItem>
                  <ListItemText>
                    <TextField
                      fullWidth
                      label='Peer Multiaddress'
                      placeholder={CONNECT_PLACEHOLDER_TEXT}
                      value={NetworkStore.pingDialogAddress}
                      onChange={e => {NetworkStore.pingDialogAddress = e.target.value}}
                      error={!isValid}
                      margin='none'
                    />
                  </ListItemText>
                </ListItem>
              </List>
            </React.Fragment>
          }
          { /* When loading */}
          { loading &&
            <div className={classes.centered}>
              <CircularProgress />
              <Typography>Ping in progress...</Typography>
            </div>
          }
          { !loading && results.length !== 0 && !error &&
            <React.Fragment>
              <DialogContentText className={classes.resultsText}>
                The address is online and reachable. This graph shows the
                latency in milliseconds of each request.
              </DialogContentText>
              <Grid container direction="column" justify="center">
                <Grid item xs={12}>
                  <LineChart width={500} height={250} data={results}>
                    <Line type="monotone" name="Latency" unit="ms" dataKey="time" stroke="#8884d8" />
                    <XAxis label="sequence" hide />
                    <YAxis unit="ms" />
                    <Tooltip/>
                  </LineChart>
                </Grid>
              </Grid>
            </React.Fragment>
          }
          { !loading && error &&
            <React.Fragment>
              <DialogContentText className={classes.resultsText}>
                An error occurred while reaching the node. This probably means
                that the peer is not reachable or it has changed the PeerID
                and/or Multiaddress. Please check your network configuration or
                contact the owner of the Peer you are trying to ping. The error
                reported is:<br />
                <br />
                {error.toString()}
              </DialogContentText>
            </React.Fragment>
          }
          </DialogContent>
          <DialogActions>
            { !loading &&
            <Button onClick={this.handleClose}>
              Close
            </Button>
            }
            { !loading && results.length === 0 && !error &&
            <Button onClick={this.startPing}>
              Start Ping
            </Button>
            }
          </DialogActions>
        </Dialog>
      </React.Fragment>
    )
  }
}


export default PingDialog