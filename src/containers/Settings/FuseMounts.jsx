import React from 'react'
import List from '@material-ui/core/List'
import BooleanSeting from '../../components/Settings/BooleanSetting';

class Experimental extends React.Component {
  render () {
    const { Mounts, patchConfig } = this.props

    return (
      <List>
        <BooleanSeting
          primary="Fuse Allow Other"
          secondary="Sets the FUSE allow other option on the mountpoint"
          checked={!!Mounts.FuseAllowOther}
          onChange={() => patchConfig('Mounts.FuseAllowOther', !Mounts.FuseAllowOther)}
        />
      </List >
    )
  }
}

export default Experimental
