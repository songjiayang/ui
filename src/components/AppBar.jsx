import React from 'react'
import { observer, inject } from 'mobx-react'
import { withSnackbar } from 'notistack'
import AppBar from '@material-ui/core/AppBar'
import Button from '@material-ui/core/Button'
import MenuIcon from '@material-ui/icons/Menu'
import CloseIcon from '@material-ui/icons/Close'
import { withStyles } from '@material-ui/core/styles'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import NotificationsIcon from '@material-ui/icons/Notifications'
import CircularProgress from '@material-ui/core/CircularProgress'
import SafeLink from './SafeLink'

import { isLatestVersion } from '../version'
import pjson from '../../package.json'

import {
  WARNING_SETTINGS,
} from '../notifications'


const TEXT_SNACK_NO_INTENRET = 'No Internet connection detected'
const TEXT_SNACK_OLD_VERSION = 'A new version of the app is available'

const SNACK_SETTINGS_OLD_VERSION = {
  ...WARNING_SETTINGS,
  autoHideDuration: 30 * 1000,
  action: <Button component={SafeLink} href='https://orion.siderus.io/' size="small">Download</Button>
}

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'
const isMac = navigator.platform.includes("Mac")
const isBeta = pjson.version.includes('beta')

@withSnackbar
@inject('ActivityStore')
@observer
class CustomAppBar extends React.Component {
  closeWindow = () => {
    window.close()
  }

  componentDidMount() {
    // If we are not online alert the user
    if(!navigator.onLine) {
      this.props.enqueueSnackbar(TEXT_SNACK_NO_INTENRET, WARNING_SETTINGS)
    }

    isLatestVersion()
    .then((isLatest) => {
      if(!isLatest){
        this.props.enqueueSnackbar(TEXT_SNACK_OLD_VERSION, SNACK_SETTINGS_OLD_VERSION)
      }
    })
  }

  render () {
    const { classes, onToggleMenu, ActivityStore } = this.props
    return (
      <AppBar position="fixed" className={classes.bar}>
        <Toolbar variant='dense'>
          <IconButton
            className={classes.menuButton}
            color="inherit"
            aria-label="Menu"
            onClick={onToggleMenu}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant='h6' color="inherit" className={classes.grow}>
            Orion { isBeta ? '(beta)' : ''}
          </Typography>
          <a href="#/activities" className={classes.activity_wrapper}>
            <IconButton className={classes.activity_button}>
              <NotificationsIcon />
            </IconButton>
            {ActivityStore.areAnyInProgress && <CircularProgress size={48} className={classes.activity_progress} />}
          </a>
          { isElectron && !isMac &&
            <IconButton
              className={classes.closeWindowButton}
              color="inherit"
              aria-label="Close Window"
              onClick={this.closeWindow}
            >
              <CloseIcon />
            </IconButton>
          }
        </Toolbar>
      </AppBar>
    )
  }
}

const styles = theme => {
  const custom = {
    bar: {
      color: '#FFFFFF',
      top: 'auto',
      flexGrow: 1,
      zIndex: theme.zIndex.modal + 1,
      paddingLeft: 0,
      background: `linear-gradient(200deg, ${theme.palette.secondary.main} 0%, ${theme.palette.primary.main} 100%)`,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 5,
    },
    closeWindowButton: {
      marginRight: -12,
      marginLeft: 5,
    },
    grow: {
      flexGrow: 1,
    },
    activity_wrapper: {
      color: '#FFFFFF',
      position: 'relative',
    },
    activity_button: {
      color: '#FFFFFF',
    },
    activity_progress: {
      color: '#FFFFFF',
      position: 'absolute',
      left: 0,
      top: 0,
      zIndex: -1,
    }
  }

  if(isElectron) {
    custom.bar['-webkit-app-region'] = 'drag'
    custom.menuButton.marginLeft = -12
    // If it is Mac
    if (isMac) {
      custom.bar.paddingLeft = 55
      if(window.innerHeight === window.screen.height) {
        custom.bar.paddingLeft = 0
      }
      custom.menuButton.marginLeft = 0
    }
  }

  custom.menuButton['-webkit-app-region'] = 'no-drag'
  custom.closeWindowButton['-webkit-app-region'] = 'no-drag'
  custom.activity_button['-webkit-app-region'] = 'no-drag'
  custom.activity_progress['-webkit-app-region'] = 'no-drag'

  return custom
}


export default withStyles(styles)(CustomAppBar)
