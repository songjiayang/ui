import React from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Checkbox from '@material-ui/core/Checkbox'
import Button from '@material-ui/core/Button'
import DeleteIcon from '@material-ui/icons/Delete'
import { withStyles } from '@material-ui/core/styles'
//

const TITLE = 'Names'
const TABLE_NAMES = [
  'IPNS',
  'Name',
  'Value',
  'Actions'
]
const REMOVE_BUTTON_LABEL = 'Remove'

const KeyList = ({
  classes,
  children,
  onSelectAll = () => { },
  onRemoveSelected = () => { },
  checked = false,
  indeterminate = false,
  selections = 0
}) => (
    <div className={classes.root}>
      <Toolbar className={selections ? classes.highlight : undefined}>
        <Typography
          color={selections ? 'default' : 'default'}
          variant='h6'
        >
          {selections ? `${selections} selected` : TITLE}
        </Typography>
        {
          selections !== 0 &&
          <div>
            <Button onClick={onRemoveSelected}>
              <DeleteIcon />
              {REMOVE_BUTTON_LABEL}
            </Button>
          </div>
        }
      </Toolbar>
      <Table className={classes.table} padding='checkbox'>
        <TableHead>
          <TableRow>
            <TableCell>
              <Checkbox
                checked={checked}
                indeterminate={indeterminate}
                onChange={onSelectAll}
              />
            </TableCell>
            <TableCell>{TABLE_NAMES[0]}</TableCell>
            <TableCell>{TABLE_NAMES[1]}</TableCell>
            <TableCell>{TABLE_NAMES[2]}</TableCell>
            <TableCell align="right">{TABLE_NAMES[3]}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            children
          }
        </TableBody>
      </Table>
    </div>
  )

const hideOverflow = {
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis'
}

const styles = theme => ({
  root: {
    overflowX: 'auto'
  },
  highlight: {
    display: 'flex',
    justifyContent: 'space-between',
    // theme.palette.type is either 'light' or 'dark'
    backgroundColor: theme.palette.secondary[theme.palette.type]
  },
  table: {
    tableLayout: 'fixed',

    '& th:nth-child(1)': { width: 48 },
    '& th:nth-child(2)': { width: '40%', ...hideOverflow },
    '& th:nth-child(3)': { width: '20%', ...hideOverflow },
    '& th:nth-child(4)': { width: '40%', ...hideOverflow },
    '& th:nth-child(5)': { width: 68 },

    '& td:nth-child(3)': hideOverflow,
    '& td:nth-child(4)': hideOverflow,
  }
})

export default withStyles(styles)(KeyList)
