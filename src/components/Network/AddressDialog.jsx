import React from 'react'
import { withRouter } from 'react-router'
import { observer, inject } from 'mobx-react'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Tooltip from '@material-ui/core/Tooltip'
import ClipboardIcon from '@material-ui/icons/FileCopy'
import FingerprintIcon from '@material-ui/icons/Fingerprint'
import copy from 'copy-to-clipboard'
import IconButton from '@material-ui/core/IconButton'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'

const OPEN_ADDRESS_DIALOG_BUTTON_LABEL = "My Address"
const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

const styles = theme => ({
  myAddressText: {
    overflow: 'hidden',
    marginRight: 20,
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  content: {
    minWidth: 500,
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  }
})

@withRouter
@withStyles(styles)
@inject('SettingsStore')
@inject('NetworkStore')
@observer
class AddressDialog extends React.Component {
  state = {
    open: false,
  }

  componentWillMount () {
    this.props.SettingsStore.fetchIPFS()
  }

  handleOpen = () => {
    this.props.SettingsStore.fetchIPFS()
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  copyToClipboard = (text) => {
    if (isElectron) {
      window.electron.remote.clipboard.writeText(text)
    } else {
      copy(text)
    }
  }

  render () {
    const { classes, SettingsStore } = this.props
    const { open } = this.state

    const addresses = SettingsStore.peerInfo.addresses
    let nonLocalHostAdresses = addresses.filter(addr =>
      !addr.includes('::1') && !addr.includes('127.0.0.1')
    )
    // reverse the array to show the public ip first
    nonLocalHostAdresses = nonLocalHostAdresses.reverse()

    return (
      <React.Fragment>
        <Button onClick={this.handleOpen}>
          <FingerprintIcon className={classes.leftIcon} />
            {OPEN_ADDRESS_DIALOG_BUTTON_LABEL}
        </Button>
        <Dialog
          open={open}
          onClose={this.handleClose}
        >
          <DialogTitle>Your IPFS Multiaddress</DialogTitle>
          <DialogContent className={classes.content}>
            <DialogContentText>
              Share the followign addresses with your friends in order to
              establish a direct connetion. This will ensure faster resolving
              and better performances between the peers. Remember that
              these values may change based on yor IP address and network
              capabilities.
            </DialogContentText>
            <List>
              {
                nonLocalHostAdresses.map(address => (
                  <ListItem key={address}>
                    <ListItemText primary={address} className={classes.myAddressText} />
                    <ListItemSecondaryAction>
                      <Tooltip title='Copy to clipboard'>
                        <IconButton
                          aria-label='Copy to clipboard'
                          onClick={() => this.copyToClipboard(address)}
                        >
                          <ClipboardIcon />
                        </IconButton>
                      </Tooltip>
                    </ListItemSecondaryAction>
                  </ListItem>
                ))
              }
            </List>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose}>
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    )
  }
}


export default AddressDialog