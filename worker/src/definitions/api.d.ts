declare type Multiaddress = string
declare type Multihash = string

interface ApiPeer {
  addr: {
    toString: () => string,
  }
  peer: {
    id: Buffer,
    _idB58String: string,
  }
}

interface ApiBandwidthStats {
  totalIn: number,
  totalOut: number,
  rateIn: number,
  rateOut: number,
}

interface ApiStat {
  Hash: Multiaddress,
  NumLinks: number,
  BlockSize: number,
  LinksSize: number,
  DataSize: number,
  CumulativeSize: number,
}

interface ApiDagNode {
  size: number,
  links: ApiDagLink[],
  data: string | Buffer | any,
  toJSON: () => any,
}

interface ApiDagLink {
  size: number,
  name: string,
  cid: Multiaddress,
  toJSON: () => any,
}

interface ApiPin {
  hash: string,
  type: string,
}

interface ApiPing {
  success: boolean
  time: number
  text?: string
}
