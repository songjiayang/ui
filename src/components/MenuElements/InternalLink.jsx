import React from 'react'
import { Link } from 'react-router-dom'
import InsertLink from '@material-ui/icons/InsertLink'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'


class InternalLink extends React.Component {
  render () {
    const { disableIcon, iconComponent, ...rest } = this.props
    let { label } = this.props

    label = label || "Link"

    const iconItem = (
      <ListItemIcon>
        { iconComponent || <InsertLink /> }
      </ListItemIcon>
    )

    return (
      <MenuItem 
        component={Link}
        {...rest}>
        
        { !disableIcon && iconItem }
        { !disableIcon && <ListItemText
          inset
          primary={label}
        />}
        { disableIcon && label }
      </MenuItem>
    )
  }

}


export default InternalLink
