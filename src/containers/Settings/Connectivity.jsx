import React from 'react'
import List from '@material-ui/core/List'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { withSnackbar } from 'notistack'
import BooleanSeting from '../../components/Settings/BooleanSetting'
import TextSetting from '../../components/Settings/TextSetting'
import SafeLink from '../../components/SafeLink'
import { resetIPFSClient } from '../../electron/shell'

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

const CORS_HELP_URL = 'https://stackoverflow.com/questions/42708251/how-to-do-cross-origin-requests-on-ipfs'
const RELOAD_NOTIFICATION_SETTINGS = { variant: 'warning', autoHideDuration: 3500 }

class Connectivity extends React.Component {
  state = {
    // newMDNSInterval: false,
    // newMaxConnections: false,
    newIPNSRecordLifetime: false,
    newIPNSRepublishPeriod: false,
    newRoutingType: false,
    newApiAddress: '',
    newGateway: false,
  }

  copyToClipboard = text => {
    const element = document.createElement('textarea')
    element.value = text
    document.body.appendChild(element)
    element.select()
    document.execCommand('copy')
    document.body.removeChild(element)
  }

  handleApiAddressChange = () => {
    const { patchSettings } = this.props
    const { newApiAddress } = this.state

    if(!newApiAddress) return

    patchSettings({ ipfsApiAddress: newApiAddress })
      .then(() => {
        // Let the Shell know we changed the address
        if(isElectron) resetIPFSClient(newApiAddress)
        // The IPFS API address has changed we must refresh the page
        this.props.enqueueSnackbar('IPFS address changed, reloading...', RELOAD_NOTIFICATION_SETTINGS)
        setTimeout(()=>{ window.location.reload() }, 2000)
      })
  }

  handleGatewayChange = () => {
    const { patchSettings } = this.props
    const { newGateway } = this.state

    if(!newGateway) return

    patchSettings({ gateway: newGateway })
  }

  render () {
    const {
      patchConfig,
      classes,
      Swarm,
      Discovery,
      Routing,
      Ipns,
      gateway,
    } = this.props

    const {
      // newMDNSInterval,
      newIPNSRecordLifetime,
      newIPNSRepublishPeriod,
      newRoutingType,
      newGateway,
      // newMaxConnections,
    } = this.state

    return (
      <React.Fragment>
        <List>
          <Typography className={classes.subheading} variant='body2' gutterBottom>
            Please make sure to enable CORS by following{' '}
            <SafeLink href={CORS_HELP_URL}>these instructions</SafeLink>
          </Typography>
          <TextSetting
            label='IPFS Gateway'
            placeholder='Specifies the full URL of the Public IPFS Gateway to use in Orion. Default: https://siderus.io'
            value={newGateway === false ? gateway : newGateway}
            fullWidth
            margin='none'
            onChange={event => this.setState({ newGateway: event.target.value })}
            onSubmit={this.handleGatewayChange}
          />
        </List>
        <List>
          <Typography className={classes.subheading} variant='subtitle1'>
            These are Daemon configuration, the IPFS node must be restarted in order to affect the changes!
          </Typography>

          <TextSetting
            label='IPNS Record lifetime'
            placeholder='Specifies the value to set on ipns records for their validity lifetime. If unset, we default to "24h".'
            value={newIPNSRecordLifetime === false ? Ipns.RecordLifetime : newIPNSRecordLifetime}
            onChange={event => this.setState({ newIPNSRecordLifetime: event.target.value })}
            onSubmit={() => patchConfig('Ipns.RecordLifetime', newIPNSRecordLifetime)}
          />
          <TextSetting
            label='IPNS Republish Period'
            placeholder='Specifies how frequently to republish ipns records. If unsed it defaults to "4h"'
            value={newIPNSRepublishPeriod === false ? Ipns.RepublishPeriod : newIPNSRepublishPeriod}
            onChange={event => this.setState({ newIPNSRepublishPeriod: event.target.value })}
            onSubmit={() => patchConfig('Ipns.RepublishPeriod', newIPNSRepublishPeriod)}
          />
          <TextSetting
            label='Routing Type'
            placeholder='Content routing mode. Can be dht, dhtclient or none. Default: "dht"'
            value={newRoutingType === false ? Routing.Type : newRoutingType}
            onChange={event => this.setState({ newRoutingType: event.target.value })}
            onSubmit={() => patchConfig('Routing.Type', newRoutingType)}
          />
          {/* <TextSetting
            label='Maximum connections'
            placeholder='Maximum amount of connections allowed ( HighWater )'
            value={newMaxConnections === false ? Swarm.ConnMgr.HighWater : newMaxConnections}
            onChange={event => this.setState({ newMaxConnections: event.target.value })}
            onSubmit={() => patchConfig('Swarm.ConnMgr.HighWater', newMaxConnections)}
          /> */}
          <BooleanSeting
            primary='Disable NAT PortMap'
            secondary='Disable NAT discovery, port mapping'
            checked={!!Swarm.DisableNatPortMap}
            onChange={() => patchConfig('Swarm.DisableNatPortMap', !Swarm.DisableNatPortMap)}
          />
          <BooleanSeting
            primary='Enable Relay Hop'
            secondary='If enabled, the node will act as an intermediate (Hop Relay) node in relay circuits for connected peers'
            checked={!!Swarm.EnableRelayHop}
            onChange={() => patchConfig('Swarm.EnableRelayHop', !Swarm.EnableRelayHop)}
          />
          <BooleanSeting
            primary='Disable Relay'
            secondary='Disables the p2p-circuit relay transport'
            checked={!!Swarm.DisableRelay}
            onChange={() => patchConfig('Swarm.DisableRelay', !Swarm.DisableRelay)}
          />
          <BooleanSeting
            primary='MDNS Discovery enabled'
            secondary='Options for multicast dns peer discovery in the network'
            checked={!!Discovery.MDNS.Enabled}
            onChange={() => patchConfig('Discovery.MDNS.Enabled', !Discovery.MDNS.Enabled)}
          />
          {/* <TextSetting
            label='MDNS Discovery Interval'
            value={newMDNSInterval === false ? Discovery.MDNS.Interval : newMDNSInterval}
            onChange={event => this.setState({ newMDNSInterval: event.target.value })}
            onSubmit={() => patchConfig('Discovery.MDNS.Interval', parseInt(newMDNSInterval))}
          /> */}
        </List>
      </React.Fragment>
    )
  }
}

const styles = theme => ({
  subheading: {
    paddingLeft: 16,
    color: theme.palette.grey[700]
  },
})

export default withStyles(styles)(withSnackbar(Connectivity))
