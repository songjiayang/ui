
import { queryGateways } from "../network"
import * as nodeIntegration from "../nodeIntegration"
import { get as getSettings, patch as patchSettings } from "../settings"
import { getApiClient } from "./api"
/**
 * Download a file locally
 *
 * @namespace node
 */
export function downloadFile(hash: string, path: string): Promise<any> {
  return nodeIntegration.saveFileToPath(hash, path)
}

/**
 * Remove one or more pins
 */
export function removeFiles(hash: string | string[]): Promise<any>  {
  return getApiClient().then((client) => client.pin.rm(hash))
}

/**
 * Add one pin
 */
export function importFile(hash: string): Promise<any> {
  return getApiClient().then((client) => client.pin.add(hash))
}

/**
 * Add a file to ipfs
 */
export function addFile(file: { name: string, content: Buffer} ): Promise<any> {
  const addRequest = {
    // create a new buffer from file.content
    // because this buffer was transmitted through postMessage, it"s .once function got lost
    content: Buffer.from(file.content),
    path: file.name,
  }

  return Promise.all([getApiClient(), getSettings()])
    .then(([c, settings]) => {
      return c.add(addRequest, {wrapWithDirectory: !settings.disableWrapping})
        /**
         * Query the gateways and return the wrapper dir or the rootFiles
         */
        .then((addResult) => {
          const rootFile = addResult[addResult.length - 1]

          if (!settings.skipGatewayQuery) {
            // Query all the uploaded files
            addResult.forEach((nfile: any) => queryGateways(nfile.hash))
            // Query the wrapper if it exists
          }

          // Return the wrapper if it exists, otherwise the rootFile
          return Promise.resolve(rootFile)
        })
    })
}

/*
 * Allows to run IPFS Files State query with {withLocal: true}
 */
export function mfsFilesStat(path: string): Promise<ApiFilesStat | any> {
  return getApiClient()
    .then((client) => client.files.stat(path, {withLocal: true}))
}
