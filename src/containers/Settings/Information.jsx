import React from 'react'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import IconButton from '@material-ui/core/IconButton'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import InfoIcon from '@material-ui/icons/Info'
import EditIcon from '@material-ui/icons/Edit'

import VersionDialog from '../../components/Settings/VersionDialog'
import ChangeAPIDialog from '../../components/Settings/ChangeAPIDialog'
import BooleanSetting from '../../components/Settings/BooleanSetting'

class Information extends React.Component {
  state = {
    isVersionDialogVisible: false,
    isChangeAPIDialogVisible: false,
  }

  handleVersionDialogToggle = () => {
    this.setState({isVersionDialogVisible: !this.state.isVersionDialogVisible})
  }

  handleChangeAPIToggle = () => {
    this.setState({isChangeAPIDialogVisible: !this.state.isChangeAPIDialogVisible})
  }

  render () {
    const {
      peerId, ipfsApiAddress,
      patchSettings, enableQuickInfoCards
    } = this.props
    const { isVersionDialogVisible, isChangeAPIDialogVisible } = this.state
    return (
      <React.Fragment>
        <List>
          <ListItem>
            <ListItemText primary="Peer Id" secondary={peerId} />
          </ListItem >
          <ListItem>
            <ListItemText primary="API address in use" secondary={ipfsApiAddress} />
            <ListItemSecondaryAction>
              <IconButton onClick={this.handleChangeAPIToggle}>
                <EditIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem >
          <BooleanSetting
            primary="Enable info and help cards"
            secondary="Enable this to show quick info cars and tips and tricks in Orion"
            checked={enableQuickInfoCards}
            onChange={() => patchSettings({ enableQuickInfoCards: !enableQuickInfoCards})}
          />
          <ListItem
            button
            onClick={this.handleVersionDialogToggle} >
              <ListItemText
                secondary="Check Orion version, support, chat links and more..."
                primary="About Orion"/>
              <ListItemSecondaryAction>
                <IconButton onClick={this.handleVersionDialogToggle}>
                  <InfoIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
        </List >
        <VersionDialog
          handleCloseButtonClick={this.handleVersionDialogToggle}
          isOpen={isVersionDialogVisible} />
        <ChangeAPIDialog
          isOpen={isChangeAPIDialogVisible}
          handleCloseButtonClick={this.handleChangeAPIToggle}
          ipfsApiAddress={ipfsApiAddress} />
      </React.Fragment>
    )
  }
}

export default Information
