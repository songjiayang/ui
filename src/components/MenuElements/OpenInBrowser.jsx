import React from 'react'
import OpenInBrowserIcon from '@material-ui/icons/OpenInBrowser'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'

import SafeLink from '../SafeLink'

class OpenInBrowser extends React.Component {
  render () {
    const { key, to, iconComponent, disableIcon } = this.props
    let { label } = this.props

    label = label || "Open in Browser"

    const iconItem = (
      <ListItemIcon>
        { iconComponent || <OpenInBrowserIcon /> }
      </ListItemIcon>
    )

    return (
      <MenuItem 
        key={key} 
        component={SafeLink} 
        href={to}>
        { !disableIcon && iconItem }
        { !disableIcon && <ListItemText
          inset
          primary={label}
        />}
        { disableIcon && label }
      </MenuItem>
    )
  }

}


export default OpenInBrowser
