import { addActivity, patchActivity, eventTrack } from "../worker/activities"
import { getSettings } from "../worker/settings"

// SUBSCRIPTIONS will map all the callback available with a specific method
// {'method': callback }
export const SUBSCRIPTIONS = {}

if (typeof window !== 'undefined' && typeof window.electron !== 'undefined') {
  const { ipcRenderer } = window.electron

  // Forward requests to the web worker and replies back to the shell
  const reply = (method, responseValue) => {
    window.electron.ipcRenderer.send('message', { method, value: responseValue })
  }

  SUBSCRIPTIONS['activities/add'] = (request) => {
    // don't reply to activities requests, the shell does not need the value
    addActivity(request).then()
  }
  SUBSCRIPTIONS['activities/patch'] = (request) => {
    // don't reply to activities requests, the shell does not need the value
    patchActivity(request).then()
  }
  SUBSCRIPTIONS['settings/get'] = (request) => {
    getSettings(request).then(val => reply('settings/get', val))
  }

  // Listen for messages from electron-shell
  ipcRenderer.on('message', (event, response) => {
    console.debug('[SPA] Message from shell: ', response)

    if (response.method in SUBSCRIPTIONS) {
      SUBSCRIPTIONS[response.method](response.value)
    }
  })
}

/**
 * Send a message to the electron shell and wait for the response
 * This will happen on the main thread (in electron) and requires the app to be opened
 * within an Electron BrowserWindow.
 *
 * @param {Object} request
 * @returns {Promise<any>}
 */
export function requestShell (request) {
  return new Promise(resolve => {
    // listen for response
    SUBSCRIPTIONS[request.method] = value => resolve(value)
    // send message
    console.log('[SPA] Message sent to electron-shell', request)
    window.electron.ipcRenderer.send('message', request)
  })
}

/**
 * Request the electron-shell to add a file/directory to IPFS from the local path.
 * Activities will be forwarded to the worker for progress tracking.
 *
 * @namespace electron
 * @param {string} path
 */
export function addFileFromPath (path) {
  eventTrack('file/add')
  return requestShell({ method: 'file/add-path', value: path })
}

export function resetIPFSClient (address) {
  return requestShell({ method: 'ipfs-client/reset', value: address })
}
