import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import QRCode from 'qrcode-react'
import SafeLink from '../SafeLink'

import SiderusLogo from '../../assets/siderus-logo.png'

const styles = () => ({
  root: {
    textAlign: 'center',
    '& a > canvas': {
      width: '100% !important',
      height: 'auto !important',
      maxHeight: '256px !important',
      maxWidth: '256px !important',
    }
  }
})

@withStyles(styles)
class CustomQRCode extends React.Component {
  render () {
    const {
      url,
      classes
    } = this.props

    return <div className={classes.root}>
      <SafeLink href={url}>
        <QRCode
          value={url}
          size={256}
          bgColor="#fafafa"
          logoWidth={256*0.125}
          logo={SiderusLogo}
        />
      </SafeLink>
    </div>
  }
}

export default CustomQRCode
