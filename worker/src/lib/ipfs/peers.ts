import { getApiClient } from "./api"

/**
 * Provides a Promise that will resolve the peer info (id, pubkye etc..)
 */
export function getPeerId(): Promise<PeerId> {
  return getApiClient().then((c) => c.id())
}

/**
 * connectTo allows easily to connect to a node by specifying a str multiaddress
 * example: connectTo("/ip4/192.168.0.22/tcp/4001/ipfs/Qm...")
 */
export function connectTo(strMultiddr: string ): Promise<any> {
  return getApiClient().then((c) => c.swarm.connect(strMultiddr))
}

/**
 * add a node to the bootstrap list by specifying a str multiaddress,
 * to easily connect to it everytime the daemon starts
 * example: addBootstrapAddr("/ip4/192.168.0.22/tcp/4001/ipfs/Qm...")
 */
export function addBootstrapAddr(strMultiddr: string): Promise<any> {
  return getApiClient().then((c) => c.bootstrap.add(strMultiddr))
}

/**
 * This function will return a promise that wants to provide the peers that
 * are owning a specific hash.
 */
export function getPeersWithObjectbyHash(hash: string ): Promise<any> {
  return getApiClient().then((c) => c.dht.findProvs(hash))
}

/**
 * Provides the amount of peers connected
 */
export function getCountPeersConnected(): Promise<number> {
  return getPeersConnected().then((a) => a.length)
}

/**
 * Provides the peers connected to
 */
export function getPeersConnected(): Promise<SimplePeer[]> {
  return getApiClient()
    .then((c) => c.swarm.peers())
    .then((peers: ApiPeer[]) => {
      // Convert all the API peers (with buffers) into usable data
      return peers.map((el) => {
        const data: SimplePeer = {
          address: el.addr.toString(),
          id: el.peer._idB58String,
        }
        return data
      }).filter((el) => !el.address.includes("p2p-circuit"))
    })
}

/**
 * Ping a specific Address for a specific amout of times.
 */
export function pingPeer(peerId: Multiaddress, count: number = 3): Promise<ApiPing[]> {
  return getApiClient().then((c) => c.ping(peerId, {count}))
}
