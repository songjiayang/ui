import React from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'

//

const hideOverflow = {
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis'
}

const styles = theme => ({
  root: {
    overflowX: 'auto'
  },
  hint: {
    padding: '20px',
  },
  highlight: {
    display: 'flex',
    justifyContent: 'space-between',
    // theme.palette.type is either 'light' or 'dark'
    backgroundColor: theme.palette.secondary[theme.palette.type]
  },
  table: {
    tableLayout: 'fixed',

    '& th:nth-child(1)': { width: '30%', ...hideOverflow },
    '& th:nth-child(2)': { width: '50%', ...hideOverflow },
    '& th:nth-child(3)': { width: '10%', ...hideOverflow },
    '& th:nth-child(4)': { width: 68 },

    '& td:nth-child(2)': hideOverflow,
  }
})

const TABLE_NAMES = [
  'Name',
  'Address',
  'Status',
  'Actions'
]

@withStyles(styles)
class PeersList extends React.Component {
  render () {
    const { classes, children } = this.props

    return (
      <div className={classes.root}>
        <Toolbar>
          <Typography variant='h6'>
            Known Peers
          </Typography>
        </Toolbar>
        <Table className={classes.table} padding='checkbox'>
          <TableHead>
            <TableRow>
              <TableCell>{TABLE_NAMES[0]}</TableCell>
              <TableCell>{TABLE_NAMES[1]}</TableCell>
              <TableCell align="right">{TABLE_NAMES[2]}</TableCell>
              <TableCell align="right">{TABLE_NAMES[3]}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            { children }
          </TableBody>
        </Table>
        { React.Children.count(children) <= 0 &&
          <Typography className={classes.hint}>
            Here you can find more information about your peer
            connectivity and a list of Known Peers.
            To improve your connection you can add
            specific IPFS peers here and Orion will connect to them
            when starting. The known peers can be friends, colleagues or
            any service that you want to be sure to be connected to.
          </Typography>
        }
      </div>
    )
  }
}

export default (PeersList)
