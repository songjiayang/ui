import React from 'react'
import { inject } from 'mobx-react'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import Slide from '@material-ui/core/Slide'

import pjson from '../../../package.json'

import DescriptionContent from './DescriptionContent'
import TrackingContent from './TrackingContent'
import WhatIsIPFSContent from './WhatIsIPFSContent'

function Transition(props) {
  return <Slide direction="down" {...props} />
}

@inject('SettingsStore')
export class WelcomeDialog extends React.Component {
  state = {
    page: 0,
    open: false,
    completed: false,
  }

  handleNext = () => {
    this.setState({ page: this.state.page +1 })
  }

  handleFinish = (allowUserTracking) => {
    this.props.onFinish(allowUserTracking)
    this.setState({open: false, completed: true})
  }

  render () {
    const { page, completed } = this.state
    let { open } = this.state
    const { classes, SettingsStore } = this.props

    // if the user has seen the latest welcome version, don't show it
    open = (SettingsStore.settings.welcomeVersionSeen !== pjson.welcomeVersion)
    // If the user completed the welcome, hide the dialog
    if(completed) open = false

    let description
    let buttons
    switch (page) {
      case 0:
        description = <DescriptionContent />
        buttons = <Button variant='outlined' color='primary' onClick={this.handleNext}>Next</Button>
        break
      case 1:
        description = <WhatIsIPFSContent />
        buttons = <Button variant='outlined' color='primary' onClick={this.handleNext}>Next</Button>
        break
      case 2:
        description = <TrackingContent />
        buttons = <React.Fragment>
          <Button onClick={() => this.handleFinish(false)}>Disable</Button>
          <Button variant='outlined' color='primary' onClick={() => this.handleFinish(true)}>
            Enable
          </Button>
        </React.Fragment>
        break
      default:
        description = <DescriptionContent />
        buttons = <Button variant='outlined' color='primary' onClick={this.handleNext}>Next</Button>
        break
    }

    return (
      <Dialog open={open} scroll='body' TransitionComponent={Transition} className={classes.dialog}>
        { description }
        <DialogActions className={classes.actions}>
          {
            buttons
          }
        </DialogActions>
      </Dialog>
    )
  }
}

const styles = theme => ({
  dialog : {
    marginTop: 'calc(3vh)'
  },
  actions: {
    '& > * + *': {
      marginLeft: theme.spacing.unit
    }
  },
})

export default withStyles(styles)(WelcomeDialog)
