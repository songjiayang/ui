/// <reference path="../../worker/src/definitions/peers.d.ts" />
/// <reference path="../../worker/src/definitions/stats.d.ts" />

import { eventTrack } from "./activities"
import { onWorkerMessage, requestWorker, waitForWorker } from "./index"

const worker: Worker | any = typeof Worker !== "undefined" ? new Worker("connectivity.js") : {}
worker.onmessage = onWorkerMessage

export function pingWorker() {
  return requestWorker(worker, { method: "ping" })
}

export function pingPeer(peerId: Multiaddress, count: number = 10): Promise<ApiPing[]> {
  eventTrack("peer/ping")
  return requestWorker(worker, { method: "peer/ping", value: { peerId, count } }, count * 5 * 1000)
}

export function getKnownPeers(): Promise<KnownPeer[]> {
  return requestWorker(worker, { method: "knownpeers/ls" }, 0)
}

export function addKnownPeer(peer) {
  eventTrack("knownpeers/add")
  return requestWorker(worker, { method: "knownpeers/add", value: peer }, 0)
}

export function removeKnownPeer(peerId: string) {
  eventTrack("knownpeers/rm")
  return requestWorker(worker, { method: "knownpeers/rm", value: peerId })
}

/**
 * Provides a Promise that will resolve the peer info (id, pubkye etc..)
 */
export function getPeer(): Promise<PeerId> {
  return requestWorker(worker, { method: "peer/get" })
}

/**
 * Provides a Promise that will resolve the Peers we are connected to
 */
export function getPeersConnected(): Promise<SimplePeer[]> {
  return requestWorker(worker, { method: "swarm/peers" }, 0)
}

/**
 * Provides a Promise that will resolve the peer info (id, pubkye etc..)
 */
export function getStatsBandiwdth(): Promise<BandwidthStats> {
  return requestWorker(worker, { method: "stats/bw" })
}

/**
 * Provides a Promise that will resolve the amout of peers we are connected to
 */
export function countPeersConnected() {
  return requestWorker(worker, { method: "swarm/peers_count" })
}

/**
 * connectTo allows easily to connect to a node by specifying a str multiaddress
 * example: connectTo("/ip4/192.168.0.22/tcp/4001/ipfs/Qm...")
 */
export function connectTo(strMultiddr: string) {
  eventTrack("settings/connectTo")
  return requestWorker(worker, { method: "swarm/connect", value: strMultiddr })
}

// Connect to Siderus Network
export function connectToSiderus() {
  return requestWorker(worker, { method: "siderus/connect" })
}

/**
 * add a node to the bootstrap list by specifying a str multiaddress,
 * to easily connect to it everytime the daemon starts
 * example: addBootstrapAddr("/ip4/192.168.0.22/tcp/4001/ipfs/Qm...")
 */
export function addBootstrapAddr(strMultiddr: string) {
  eventTrack("settings/addBootstrap")
  return requestWorker(worker, { method: "bootstrap/add", value: strMultiddr })
}

export function wait() {
  return waitForWorker(worker, "settings")
}
