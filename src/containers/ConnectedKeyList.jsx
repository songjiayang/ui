import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'

import { withSnackbar } from 'notistack'
import {
  COMPLETED_SETTINGS,
  STARTED_SETTINGS
} from '../notifications'
//
import KeyList from '../components/Keys/KeyList'
import KeyElement from '../components/Keys/KeyElement'

import { removeKey, renameKey } from '../worker/keys'
import KeyUpdateDialog from '../components/Keys/KeyUpdateDialog'

const NOTE_RM_KEY_STARTED_TEXT = 'Removing the key'
const NOTE_RM_KEY_COMPLETED_TEXT = 'Key removed'
const NOTE_RM_KEYS_COMPLETED_TEXT = 'Keys removed'

class ConnectedKeyList extends React.Component {
  state = {
    selectedKeys: [],

    renameDialogOpen: false,
    oldName: '',
    newName: '',
    isNewNameValid: true,

    updateDialogOpen: false,
    keyToUpdate: null
  }

  handleRemoveSelected = () => {
    console.log('Removing all the selected keys')

    const { list } = this.props
    const { selectedKeys } = this.state
    const promises = []

    list.forEach(key => {
      if (selectedKeys.includes(key.id)) {
        promises.push(removeKey(key.name))
      }
    })

    this.setState({ selectedKeys: [] })
    Promise.all(promises)
      .then(() => {
        this.props.enqueueSnackbar(NOTE_RM_KEYS_COMPLETED_TEXT, COMPLETED_SETTINGS)
      })
  }

  handleRename = (id) => {
    const key = this.props.list.find(key => key.id === id)
    this.handleEditOpen(key.name)
  }

  handleUpdate = (id) => {
    const key = this.props.list.find(key => key.id === id)
    this.setState({ updateDialogOpen: true, keyToUpdate: key })
  }

  handleUpdateClose = () => this.setState({ updateDialogOpen: false, keyToUpdate: null })

  handlePublish = () => {
    this.handleUpdateClose()
  }

  handleDelete = (id) => {
    const key = this.props.list.find(key => key.id === id)

    this.props.enqueueSnackbar(NOTE_RM_KEY_STARTED_TEXT, STARTED_SETTINGS)
    removeKey(key.name)
      .then(() => {
        this.removeSelection(id)
        this.props.enqueueSnackbar(NOTE_RM_KEY_COMPLETED_TEXT, COMPLETED_SETTINGS)
      })
  }

  isRowSelected = id => this.state.selectedKeys.indexOf(id) !== -1
  areAllSelected = () => this.state.selectedKeys.length !== 0 && this.state.selectedKeys.length === this.props.list.length
  // if there are some selections, but not all, we need to show an indeterminate checkbox
  hasSomeSelections = () => this.state.selectedKeys.length > 0 && !this.areAllSelected()

  handleRowSelect = (id) => {
    const isSelected = this.isRowSelected(id)

    let nextValue

    if (isSelected) {
      nextValue = this.state.selectedKeys.filter(x => x !== id)
    } else {
      nextValue = [...this.state.selectedKeys, id]
    }

    this.setState({ selectedKeys: nextValue })
  }

  handleSelectAll = () => {
    const { list } = this.props

    // all selected
    if (this.areAllSelected()) {
      // reset
      this.setState({ selectedKeys: [] })
    } else {
      // select all
      const nextValue = list.map(pin => pin.id)
      this.setState({ selectedKeys: nextValue })
    }
  }

  removeSelection = (id) => {
    const nextValue = this.state.selectedKeys.filter(x => id === x)
    this.setState({ selectedKeys: nextValue })
  }

  render () {
    const { list, valuesMap, gateway } = this.props
    const { selectedKeys, updateDialogOpen, keyToUpdate } = this.state

    const areValuesLoading = Object.keys(valuesMap).length === 0

    return (
      <React.Fragment>
        <KeyList
          onSelectAll={this.handleSelectAll}
          checked={this.areAllSelected()}
          indeterminate={this.hasSomeSelections()}
          selections={selectedKeys.length}
          onRemoveSelected={this.handleRemoveSelected}
        >
          {
            list.map(key =>
              <KeyElement
                {...key}
                key={key.id}
                isSelected={this.isRowSelected(key.id)}
                onSelect={this.handleRowSelect}
                onDelete={this.handleDelete}
                onRename={this.handleRename}
                onUpdate={this.handleUpdate}
                gateway={gateway}
                value={areValuesLoading ? 'Loading...' : valuesMap[key.id]}
              />
            )
          }
        </KeyList>
        {this.renderRenameDialog()}
        <KeyUpdateDialog
          open={updateDialogOpen}
          keys={list}
          preselectedKeyName={keyToUpdate ? keyToUpdate.name : undefined}
          onClose={this.handleUpdateClose}
          onSubmit={this.handlePublish}
        />
      </React.Fragment>
    )
  }

  handleEditClose = () => {
    this.setState({
      renameDialogOpen: false,
      oldName: '',
      newName: '',
      isNewNameValid: true
    })
  }

  handleEditOpen = (oldName) => {
    this.setState({ renameDialogOpen: true, oldName })
  }

  onRenameInputChange = (event) => {
    const newName = event.target.value
    const isNewNameValid = typeof newName === 'string' && newName.length > 0

    this.setState({ newName, isNewNameValid })
  }

  handleEditCompleted = () => {
    const { oldName, newName, isNewNameValid } = this.state

    if (newName && isNewNameValid) {
      renameKey(oldName, newName)
      this.handleEditClose()
    }
  }

  renderRenameDialog = () => (
    <Dialog
      open={this.state.renameDialogOpen}
      onClose={this.handleEditClose}
    >
      <DialogTitle>Edit key: {this.state.oldName}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Insert the new name of this key:
        </DialogContentText>
        <TextField
          error={!this.state.isNewNameValid}
          value={this.state.newName}
          onChange={this.onRenameInputChange}
          autoFocus
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={this.handleEditClose}>
          Cancel
        </Button>
        <Button
          onClick={this.handleEditCompleted}
          color="primary"
          variant='outlined'
          disabled={!this.state.isNewNameValid}
        >
          Rename
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default withSnackbar(ConnectedKeyList)
