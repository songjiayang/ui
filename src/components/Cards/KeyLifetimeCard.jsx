import React from 'react'
import { observer, inject } from 'mobx-react'
import AccessTimeIcon from '@material-ui/icons/AccessTime'
import Typography from '@material-ui/core/Typography'

import GenericCard from './GenericCard'

@inject('SettingsStore')
@observer
export default class KeyLifetimeCard extends React.Component {
  onClick = () => {
    this.props.SettingsStore.fetch()
  }

  render (){
    const { SettingsStore } = this.props
    const { RecordLifetime } = SettingsStore.ipfsConfig.Ipns

    return (
      <GenericCard
        Icon={AccessTimeIcon}
        tooltip="Every IPNS key has a lifetime in the network"
        title="Lifetime Period"
        style={{width: 230}}
        onClick={this.onClick}
      >
        <Typography variant="h5" component="h3">
            {RecordLifetime || "24h"}
        </Typography>
      </GenericCard>
    )
  }
}