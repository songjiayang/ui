import React from 'react'
import Switch from '@material-ui/core/Switch'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import { withSnackbar } from 'notistack'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import LinearProgress from '@material-ui/core/LinearProgress'

import InfoIcon from '@material-ui/icons/Info'
import { resetConfiguration, clearCache, getCacheSize, runGarbageCollection } from '../../worker/settings'
import TrackingContent from '../../components/Welcome/TrackingContent'

import {
  COMPLETED_SETTINGS,
  STARTED_SETTINGS,
} from '../../notifications'


class Privacy extends React.Component {
  state = {
    trackingInfoOpen: false,
    resetOpen: false,
    resetting: false,
    cacheSize: null
  }

  componentDidMount () {
    this.updateCacheSize()
  }

  updateCacheSize = () => {
    getCacheSize()
      .then(cacheSize => {
        this.setState({ cacheSize })
      })
  }

  openTrackingInfoDialog = () => this.setState({ trackingInfoOpen: true })
  closeTrackingInfoDialog = () => this.setState({ trackingInfoOpen: false })

  openResetDialog = () => this.setState({ resetOpen: true })
  closeResetDialog = () => this.setState({ resetOpen: false })

  handleUserTrackingChange = () => {
    const nextValue = !this.props.allowUserTracking
    this.props.patchSettings({ allowUserTracking: nextValue })
  }

  handleDeleteConfiguration = () => {
    this.props.enqueueSnackbar('Resetting configuration...', STARTED_SETTINGS)
    this.setState({ resetting: true })
    resetConfiguration()
      .then(() => {
        window.location = window.location.origin + window.location.pathname
      })
  }

  handleClearCache = () => {
    this.props.enqueueSnackbar('Clearing cache...', STARTED_SETTINGS)
    clearCache()
    .then(() => this.props.enqueueSnackbar('Cache cleared', COMPLETED_SETTINGS))
    .then(this.updateCacheSize)
  }

  handleRunGC = () => {
    this.props.enqueueSnackbar('Collecting Garbage...', STARTED_SETTINGS)
    runGarbageCollection()
    .then(() => this.props.enqueueSnackbar('Garbage collected', COMPLETED_SETTINGS))
    .then(this.updateCacheSize)
  }

  render () {
    const { trackingInfoOpen, resetOpen, resetting, cacheSize } = this.state
    const { allowUserTracking } = this.props

    return (
      <React.Fragment>
        <List>
          <ListItem
            button
            onClick={this.handleUserTrackingChange}
          >
            <ListItemText
              primary="Telemetry"
              secondary="Send anonymized and basic stats to help improve Orion"
            />
            <ListItemSecondaryAction>
              <IconButton onClick={this.openTrackingInfoDialog}>
                <InfoIcon />
              </IconButton>
              <Switch
                checked={allowUserTracking}
                onChange={this.handleUserTrackingChange}
              />
            </ListItemSecondaryAction>
          </ListItem>
          <ListItem
            button
            onClick={this.handleClearCache}
          >
            <ListItemText
              primary="Clear Orion cache"
              secondary={`The current internal cache contains ${cacheSize || 0} elements`}
            />
          </ListItem>
          <ListItem
            button
            onClick={this.handleRunGC}
          >
            <ListItemText
              primary="Run Garbage Collector"
              secondary="Removes content that is not used pinned/saved."
            />
          </ListItem>
          <ListItem
            button
            onClick={this.openResetDialog}
          >
            <ListItemText primary="Reset configuration" />
          </ListItem >
        </List>
        <Dialog
          open={trackingInfoOpen}
          onClose={this.closeTrackingInfoDialog}
        >
          <TrackingContent />
          <DialogActions>
            <Button onClick={this.closeTrackingInfoDialog} color="primary">
              CLOSE
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={resetOpen}
          onClose={this.closeResetDialog}
        >
          {
            resetting &&
            <LinearProgress variant="query" />
          }
          <DialogTitle>Are you sure you want to reset everything?</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              The configuration will be reset to default values. The cache will be cleared but your
              files, objects and names will not be removed. <br />
              <br />
              This action cannot be undone.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeResetDialog} disabled={resetting}>
              Cancel
                </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={this.handleDeleteConfiguration}
              disabled={resetting}
            >Reset</Button>
          </DialogActions>
        </Dialog>
      </React.Fragment >
    )
  }
}

export default withSnackbar(Privacy)
