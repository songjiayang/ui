import React from 'react'
import { withRouter } from 'react-router'
import NavLink from 'react-router-dom/NavLink'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListIcon from '@material-ui/icons/List'
import SettingsIcon from '@material-ui/icons/Settings'
import NotificationsIcon from '@material-ui/icons/Notifications'
import LeakAddIcon from '@material-ui/icons/LeakAdd'
import TextFieldsIcon from '@material-ui/icons/TextFields'
import { withStyles } from '@material-ui/core/styles'

const styles = {
  drawerPaper: {
    paddingTop: 48,
    width: 240
  }
}

@withRouter
@withStyles(styles)
export default class AppMenu extends React.Component {

  render () {
    const { classes, open, onCloseMenu } = this.props

    return (
      <Drawer
        open={open}
        onClose={onCloseMenu}
        anchor="left"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <List component="nav">
          <ListItem
            button
            component={NavLink}
            to="/"
            onClick={onCloseMenu}
          >
            <ListItemIcon>
              <ListIcon />
            </ListItemIcon>
            <ListItemText primary="Files" />
          </ListItem>
          <ListItem
            button
            component={NavLink}
            to="/names"
            onClick={onCloseMenu}
          >
            <ListItemIcon>
              <TextFieldsIcon />
            </ListItemIcon>
            <ListItemText primary="Names" />
          </ListItem>
          <ListItem
            button
            component={NavLink}
            to="/network"
            onClick={onCloseMenu}
          >
            <ListItemIcon>
              <LeakAddIcon />
            </ListItemIcon>
            <ListItemText primary="Network" />
          </ListItem>
          <ListItem
            button
            component={NavLink}
            to="/settings"
            onClick={onCloseMenu}
          >
            <ListItemIcon>
              <SettingsIcon />
            </ListItemIcon>
            <ListItemText primary="Settings" />
          </ListItem>
          <ListItem
            button
            component={NavLink}
            to="/activities"
            onClick={onCloseMenu}
          >
            <ListItemIcon>
              <NotificationsIcon />
            </ListItemIcon>
            <ListItemText primary="Activities" />
          </ListItem>
        </List>
      </Drawer>
    )
  }
}
