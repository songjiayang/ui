import React from 'react'
import Card from '@material-ui/core/Card'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

import CardIcon from './CardIcon'

const styles = {
  main: {
    flex: '1',
    marginTop: 15,
  },
  card: {
    overflow: 'inherit',
    textAlign: 'right',
    padding: 16,
    minHeight: 90,
  },
}

@withStyles(styles)
export default class GenericCard extends React.Component {
  render (){
    const { classes, Icon, title, children, tooltip="", ...rest } = this.props
    return (
      <div className={classes.main} {...rest}>
        <CardIcon Icon={Icon} background="black" tooltip={tooltip}/>
        <Card className={classes.card}>
            { title &&
              <Typography color="textSecondary">
                { title }
              </Typography>
            }
            { children }
        </Card>
      </div>
    )
  }
}