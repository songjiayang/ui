import React from 'react'
import { observer, inject } from 'mobx-react'

import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { HashLink } from 'react-router-hash-link'
import { withSnackbar } from 'notistack'
//

import FilesTab from './Files'
import ConnectivityTab from './Connectivity'
import InformationTab from './Information'
import PrivacyTab from './Privacy'
import ExperimentalTab from './Experimental'

import { patchSettings, patchIPFSConfig } from '../../worker/settings'
import { eventTrack } from '../../worker/activities'

import {
  COMPLETED_SETTINGS,
} from '../../notifications'


const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

const RESTART_NOTIFICATION_TEXT = isElectron
  ? 'Please restart the app to apply the changes'
  : 'Please restart IPFS to apply the changes'
const RESTART_NOTIFICATION_SETTINGS = { variant: 'warning', autoHideDuration: 5000 }
const RESTART_NOTIFICATION_DELAY = 500

const SUCCESS_NOTIFICATION_TEXT = 'Settings updated'

@inject('SettingsStore')
@observer
class Settings extends React.Component {
  componentDidMount () {
    eventTrack('settings')

    this.props.SettingsStore.fetch()
  }

  handleSettingsPatch = (partialSettings) =>
    // update the state after the settings are updated
    patchSettings(partialSettings)
      .then(() => {
        this.props.enqueueSnackbar(SUCCESS_NOTIFICATION_TEXT, COMPLETED_SETTINGS)
        this.props.SettingsStore.fetch()
      })


  handleConfigPatch = (key, value) => {
    patchIPFSConfig(key, value)
      .then(() => {
        this.props.enqueueSnackbar(SUCCESS_NOTIFICATION_TEXT, COMPLETED_SETTINGS)
        setTimeout(() =>{
          this.props.enqueueSnackbar(RESTART_NOTIFICATION_TEXT, RESTART_NOTIFICATION_SETTINGS)
        }, RESTART_NOTIFICATION_DELAY)
        this.props.SettingsStore.fetch()
      })
  }

  render () {
    const { classes, SettingsStore } = this.props

    return (
      <React.Fragment>
      { !SettingsStore.loading && <div className={classes.root}>
        <Drawer
          variant='permanent'
          anchor='left'
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <List component='nav'>
            <ListItem button component={HashLink} to='/settings#information'>
              <ListItemText primary='Information' />
            </ListItem>
            <ListItem button component={HashLink} to='/settings#files'>
              <ListItemText primary='Files' />
            </ListItem>
            <ListItem button component={HashLink} to='/settings#connectivity'>
              <ListItemText primary='Connectivity' />
            </ListItem>
            <ListItem button component={HashLink} to='/settings#advanced'>
              <ListItemText primary='Advanced' />
            </ListItem>
            <ListItem button component={HashLink} to='/settings#privacy'>
              <ListItemText primary='Privacy' />
            </ListItem>
          </List>
        </Drawer>
        <div className={classes.content}>
          <Grid container spacing={16}>
            <Grid item xs={12}>
              <Typography
                id='information'
                variant='subtitle1'
                gutterBottom
              >Information</Typography>
              <Paper elevation={4}>
                <InformationTab
                  peerId={SettingsStore.peerInfo.id}
                  patchSettings={this.handleSettingsPatch}
                  enableQuickInfoCards={SettingsStore.settings.enableQuickInfoCards}
                  ipfsApiAddress={SettingsStore.settings.ipfsApiAddress}
                />
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Typography
                id='files'
                variant='subtitle1'
                gutterBottom
              >Files</Typography>
              <Paper elevation={4}>
                <FilesTab
                  Datastore={SettingsStore.ipfsConfig.Datastore}
                  skipGatewayQuery={SettingsStore.settings.skipGatewayQuery}
                  disableWrapping={SettingsStore.settings.disableWrapping}
                  patchSettings={this.handleSettingsPatch}
                  patchConfig={this.handleConfigPatch}
                  Gateway={SettingsStore.ipfsConfig.Gateway}
                />
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Typography
                id='connectivity'
                variant='subtitle1'
                gutterBottom
              >Connectivity</Typography>
              <Paper elevation={4}>
                <ConnectivityTab
                  addresses={SettingsStore.peerInfo.addresses}
                  gateway={SettingsStore.settings.gateway}
                  Swarm={SettingsStore.ipfsConfig.Swarm}
                  Routing={SettingsStore.ipfsConfig.Routing}
                  Ipns={SettingsStore.ipfsConfig.Ipns}
                  Discovery={SettingsStore.ipfsConfig.Discovery}
                  patchSettings={this.handleSettingsPatch}
                  patchConfig={this.handleConfigPatch}
                />
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Typography
                id='advanced'
                variant='subtitle1'
                gutterBottom
              >Experimental / Advanced</Typography>
              <Paper elevation={4}>
                <ExperimentalTab
                  Experimental={SettingsStore.ipfsConfig.Experimental}
                  patchConfig={this.handleConfigPatch}
                />
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Typography
                id='privacy'
                variant='subtitle1'
                gutterBottom
              >Privacy</Typography>
              <Paper elevation={4}>
                <PrivacyTab
                  allowUserTracking={SettingsStore.settings.allowUserTracking}
                  patchSettings={this.handleSettingsPatch}
                />
              </Paper>
            </Grid>
          </Grid>
        </div>
      </div> }
      </React.Fragment>
    )
  }
}

const styles = {
  root: {},
  drawerPaper: {
    paddingTop: 48,
    width: 200
  },
  content: {
    padding: 12,
    marginLeft: 200
  }
}

export default withStyles(styles)(withSnackbar(Settings))
