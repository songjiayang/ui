import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { observer, inject } from 'mobx-react'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import { withRouter } from 'react-router'
import { withSnackbar } from 'notistack'
import Hidden from '@material-ui/core/Hidden'

import LoadingOverlay from '../components/LoadingOverlay'
import PeersList from '../components/Network/PeersList'
import PeerElement from '../components/Network/PeerElement'
import SafeLink from '../components/SafeLink'

import AddressDialog from '../components/Network/AddressDialog'
import ConnectDialog from '../components/Network/ConnectDialog'
import PingDialog from '../components/Network/PingDialog'

import Toolbar from '../components/ToolBar/Toolbar'

import PeerCountCard from '../components/Cards/PeerCountCard'
import BandwidthCard from '../components/Cards/BandwidthCard'
import HelpCard from '../components/Cards/HelpCard'
import CardHeader from '../components/Cards/CardHeader'

import { eventTrack } from '../worker/activities'

const styles = {}

@withStyles(styles)
@withRouter
@withSnackbar
@inject('NetworkStore')
@observer
export default class Network extends React.Component {
  componentDidMount () {
    eventTrack('networking')

    this.props.NetworkStore.fetchKnownPeers()
    this.props.NetworkStore.startLoop()
  }

  componentWillUnmount() {
    this.props.NetworkStore.stopLoop()
  }

  render () {
    const { NetworkStore } = this.props

    return (
      <React.Fragment>
        <LoadingOverlay loading={NetworkStore.loading}>
          <Toolbar>
            <AddressDialog />
            <ConnectDialog />
            <PingDialog />
          </Toolbar>
          <CardHeader>
            <PeerCountCard />
            <BandwidthCard />
            <Hidden smDown>
              <HelpCard tips={[
                "Did you know that you can disable Cards like this one from the settings?",
                "Did you know that even when unused, your IPFS peer is helping the network?",
                "Siderus' public gateway is faster because Orion is connected to it",
                "Adding a Known Peer will improve the connection when sharing content",
                <React.Fragment>
                  Do you like Orion? <br />
                  Follow Siderus on twitter:{' '}
                  <SafeLink href="https://twitter.com/IPFSRocks">@IPFSRocks</SafeLink>!
                </React.Fragment>,
                <React.Fragment>
                  Do you need help? Check{' '}
                  <SafeLink href="https://orion.siderus.io/">orion.siderus.io</SafeLink> for more info!
                </React.Fragment>,
              ]}/>
            </Hidden>
          </CardHeader>
          <Grid container spacing={16}>
            <Grid item xs={12}>
              <Paper elevation={4}>
                <PeersList >
                  { NetworkStore.knownPeers.map((el, index) => <PeerElement
                    peer={el} key={index} id={index}
                  />)}
                </PeersList>
              </Paper>
            </Grid>
          </Grid>
        </LoadingOverlay>
      </React.Fragment>
    )
  }
}