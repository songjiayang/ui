declare enum ActivityTypes {
  /**
   * Add a file to the ipfs repository
   */
  FILE_ADD = "file/add",
  /**
  * Will "import" an object recursively, by pinning it to the
  * repository.
  */
  IMPORT_FROM_HASH = "import-from-hash",
  /**
   * This will return to the client an url to open in the browser
   */
  OPEN_IN_BROWSER = "open-in-browser",
}

declare enum ActivityStates {
  COMPLETED = "completed",
  FAILED = "failed",
  IN_PROGRESS = "in progress",
  INTERRUPTED = "interrupted",
}

interface Activity {
  uuid?: string,
  type?: string | ActivityTypes, // ActivityTypes
  name?: string,
  status: string | ActivityStates, // ActivityStates
  progress?: any,
  timestamp?: Date,
  data ?: any, // Extra data to enrich the notification
  link ?: string, // Optional Link to point to for interaction
}
