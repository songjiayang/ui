import React from 'react'

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

class SafeLink extends React.Component {
  handleOnClick = () => {
    if(isElectron) window.electron.shell.openExternal(this.props.href)
    if(this.props.onClick) this.props.onClick()
  }

  render() {
    const {children, ...rest} = this.props

    // if it is not electron, render just the link
    if (!isElectron) return (
      <a target='_blank' rel='noopener noreferrer' {...rest}>
        {children}
      </a>
    )
     
    // If it is electron
    delete rest.href
    delete rest.target
    delete rest.onClick
    return (
      <a 
        role='link' tabIndex='0' onKeyDown={this.handleOnClick} 
        onClick={this.handleOnClick} {...rest}
      >
        {children}
      </a>
    )
  }
}

export default SafeLink
