import React from 'react'
import { withRouter } from 'react-router'
import SaveAlt from '@material-ui/icons/SaveAlt'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import LinearProgress from '@material-ui/core/LinearProgress'
import Typography from '@material-ui/core/Typography'
// FIXME https://github.com/nozzle/react-static/issues/816
import { ipnsPath as isIPNS } from 'is-ipfs'

import { resolveKeyValues } from "../../worker/keys"


const RESOLVE_BUTTON_LABEL = "Resolve Key"

const styles = theme => ({
  content: {
    minWidth: 500,
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  }
})


@withRouter
@withStyles(styles)
class ResolveKey extends React.Component {
  initialState = {
    open: false,
    key: '',
    isValid: true,
    value: null,

    lastHandledRequestId: -1
  }

  state = this.initialState

  handleOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({
      open: false,
      key: '',
      isValid: true,
      value: null,
     })
  }

  handleSubmit = () => {
    const { key, isValid, value } = this.state

    if (key && isValid) {
      this.props.history.push(`/properties/${value}`)
    }
  }

  onInputChange = (event) => {
    const key = event.target.value
    const isValid = isIPNS(key)

    this.setState({ key, isValid, value: null })

    if (isValid) {
      resolveKeyValues([key]).then(result => {
        const value = result[key].replace("/ipfs/", "")
        this.setState({ value })
      })
    }
  }

  render () {
    const { classes } = this.props
    const { open, key, isValid, value } = this.state

    const loading = isValid && key && (value === null)
    return (
      <React.Fragment>
        <Button onClick={this.handleOpen}>
          <SaveAlt className={classes.leftIcon} />
          {RESOLVE_BUTTON_LABEL}
        </Button>
        <Dialog
          open={open}
          onClose={this.handleClose}
          disableEscapeKeyDown={!!loading} disableBackdropClick={!!loading}
        >
          {
            loading &&
            <LinearProgress variant='query' />
          }
          <DialogTitle>Import IPNS Key / Name</DialogTitle>
          <DialogContent className={classes.content}>
            <DialogContentText>
              Insert the IPNS key you would like to add to your peer:
            </DialogContentText>
            <TextField
              error={!isValid}
              value={key}
              onChange={this.onInputChange}
              autoFocus
              fullWidth
              placeholder='/ipns/Qma1B2C4d4...'
            />
            <Typography>
              Note: This request might take up to 1 minute to resolve the value
            </Typography>
            {
              ( isValid && key &&
                <Typography>
                  {'Current Value: '}
                  {value ? `${value}` : 'Loading...'}
                </Typography>
              ) ||
                <Typography>
                  {'Insert a valid value in order to resolve'}
                </Typography>
            }
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose}>
              Cancel
            </Button>
            <Button
              onClick={this.handleSubmit}
              color='primary'
              variant='outlined'
              disabled={!value}
            >
              Open
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    )
  }
}


export default ResolveKey