import React from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Checkbox from '@material-ui/core/Checkbox'
import Button from '@material-ui/core/Button'
import DeleteIcon from '@material-ui/icons/Delete'
import { withStyles } from '@material-ui/core/styles'
import Hidden from '@material-ui/core/Hidden'
//

const TITLE = 'Files'
const TABLE_NAMES = [
  '', // Type e.g. directory/file
  'Files',
  'Description',
  'Hash',
  'Size',
  'Actions'
]
const REMOVE_BUTTON_LABEL = 'Remove'

const StorageList = ({
  classes,
  children,
  onSelectAll = () => { },
  onRemoveSelected = () => { },
  checked = false,
  indeterminate = false,
  selections = 0,
  title
}) => (
    <div className={classes.root}>
      <Toolbar className={selections ? classes.highlight : undefined}>
        <Typography
          color={selections ? 'default' : 'default'}
          variant='h6'
        >
          {selections ? `${selections} selected` : (title || TITLE)}
        </Typography>
        {
          selections !== 0 &&
          <div>
            <Button onClick={onRemoveSelected}>
              <DeleteIcon />
              {REMOVE_BUTTON_LABEL}
            </Button>
          </div>
        }
      </Toolbar>
      <Table className={classes.table} padding='checkbox'>
        <TableHead>
          <TableRow>
            <TableCell>
              <Checkbox
                checked={checked}
                indeterminate={indeterminate}
                onChange={onSelectAll}
              />
            </TableCell>
            <TableCell>{TABLE_NAMES[0]}</TableCell>
            <TableCell>{TABLE_NAMES[1]}</TableCell>
            <TableCell>{TABLE_NAMES[2]}</TableCell>
            <Hidden xsDown><TableCell>{TABLE_NAMES[3]}</TableCell></Hidden>
            <Hidden xsDown><TableCell align='right'>{TABLE_NAMES[4]}</TableCell></Hidden>
            <TableCell align='right'>{TABLE_NAMES[5]}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            children
          }
        </TableBody>
      </Table>
    </div>
  )

const hideOverflow = {
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis'
}

const styles = theme => ({
  root: {
    overflowX: 'auto'
  },
  highlight: {
    display: 'flex',
    justifyContent: 'space-between',
    // theme.palette.type is either 'light' or 'dark'
    backgroundColor: theme.palette.secondary[theme.palette.type]
  },
  table: {
    tableLayout: 'fixed',

    '& th:nth-child(1)': { width: 48 }, // selector
    '& th:nth-child(2)': { width: 24 }, // type
    '& th:nth-child(3)': { width: '40%', ...hideOverflow }, // Files
    '& th:nth-child(4)': { width: '40%', ...hideOverflow }, // Description
    '& th:nth-child(5)': { width: '50%', ...hideOverflow }, // Hash
    '& th:nth-child(6)': { width: 100 }, // Size
    '& th:nth-child(7)': { width: 68 }, // Action

    '& td:nth-child(2) svg': {
      // looks to me like the icon is a bit higher than the checkbox
      paddingTop: 3,
    },

    '& td:nth-child(3)': hideOverflow,
    '& td:nth-child(4)': hideOverflow,
    '& td:nth-child(5)': hideOverflow,
  }
})

export default withStyles(styles)(StorageList)
