import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { observer, inject } from 'mobx-react'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import { withRouter } from 'react-router'
import { withSnackbar } from 'notistack'
import TablePagination from '@material-ui/core/TablePagination'
import Hidden from '@material-ui/core/Hidden'

import ConnectedStorageList from './ConnectedStorageList'

import AddFile from '../components/StorageList/AddFile'
import AddFileElectron from '../components/StorageList/AddFileElectron'
import ImportFile from '../components/StorageList/ImportFile'
import SearchPaper from '../components/SearchPaper'
import SafeLink from '../components/SafeLink'

import { addFile } from '../worker/files'
import { eventTrack } from '../worker/activities'

import {
  STARTED_SETTINGS,
  COMPLETED_SETTINGS,
} from '../notifications'

import HelpCard from '../components/Cards/HelpCard'
import CardHeader from '../components/Cards/CardHeader'
import RepoSizeCard from '../components/Cards/RepoSizeCard'
import PeerCountCard from '../components/Cards/PeerCountCard'

import {
  addFileFromPath
} from '../electron/shell'

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

const NOTE_IMP_FILE_STARTED_TEXT = 'Importing files...'
const NOTE_ADD_FILE_STARTED_TEXT = 'Adding files...'
const NOTE_ADD_FILE_COMPLETED_TEXT = 'Adding files completed'

const styles = {
  topActions: {},
  customToolbar: {
    minHeight: 64,
  }
}

@withStyles(styles)
@withRouter
@withSnackbar
@inject('SettingsStore')
@inject('FilesStore')
@inject('NetworkStore')
@observer
export default class FilesView extends React.Component {
  state = {
    searchResults: [],
    rowsPerPage: 20,
    page: 0,
    isSearching: false
  }

  componentDidMount () {
    eventTrack('file')
    setupAddAppOnDrop((fileList) => {
      // Converts FileList to Array of Files
      const files = Array.from(fileList)
      if(!files || files.length === 0) {
        console.warn('[Files View] Drag and drop resulted empty')
        return
      }

      if (!isElectron) {
        return this.handleFilesChangeInHTML(files)
      }

      // within electron, we only need the paths
      this.handleFilesChangeInElectron(files)
    })

    this.props.FilesStore.startLoop()
    this.props.NetworkStore.startLoop(5000) // refresh network every 5 seconds
    this.props.SettingsStore.startLoop(7000) // refresh settings every 7 seconds
  }

  componentWillUnmount () {
    this.props.FilesStore.stopLoop()
    this.props.NetworkStore.stopLoop()
    this.props.SettingsStore.stopLoop()
  }

  onSearchChange = (value) => {
    const searchResults = this.props.FilesStore.search(value)
    this.setState({ searchResults })
  }

  onSearchFocus = () => {
    this.setState({ isSearching: true })
  }

  onSearchUnFocus = () => {
    this.setState({ isSearching: false })
  }

  /**
   * https://developer.mozilla.org/en-US/docs/Web/API/FileList
   * @param {FileList} files
   */
  handleFilesChangeInHTML = (files) => {
    console.debug('[handleFilesChangeInHTML] adding:', files)
    if (!files) {
      console.warn('Files provided is empty.')
      return
    }

    this.props.enqueueSnackbar(NOTE_ADD_FILE_STARTED_TEXT, STARTED_SETTINGS)
    return Promise.all(files.map(addFile))
      .then(() => {
        // const hashes = result.map(x => x.hash)
        this.props.enqueueSnackbar(NOTE_ADD_FILE_COMPLETED_TEXT, COMPLETED_SETTINGS)
      })
  }

  handleFilesChangeInElectron = (files) => {
    // In case the user selected no files/directories
    if(!files) return
    const paths = files.map((x) => {
      // Note that on Electron if we are using a Dialog we will get a string
      if(typeof x === "string") return x
      // If we are not using the dialog (but drag and drop) we need the path
      return x.path
    })

    this.props.enqueueSnackbar(NOTE_ADD_FILE_STARTED_TEXT, STARTED_SETTINGS)
    Promise.all(paths.map(addFileFromPath))
      .then(() => {
        // const hashes = result.map(x => x.hash)
        this.props.enqueueSnackbar(NOTE_ADD_FILE_COMPLETED_TEXT, COMPLETED_SETTINGS)
      })
  }

  handleFileImport = (hash) => {
    this.props.enqueueSnackbar(NOTE_IMP_FILE_STARTED_TEXT, STARTED_SETTINGS)

    this.props.FilesStore.import(hash)
      .then(() => {
        this.props.enqueueSnackbar(NOTE_ADD_FILE_COMPLETED_TEXT, COMPLETED_SETTINGS)
      })
  }

  handleChangePage = (event, page) => {
    this.setState({ page })
  }

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value })
  }

  render () {
    const { FilesStore, classes } = this.props
    const { searchResults, isSearching, rowsPerPage, page } = this.state

    const data = isSearching ? searchResults : FilesStore.pins
    const pagedData = data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
    return (
      <React.Fragment>
        <Grid className={classes.customToolbar} container spacing={16}>
          <Grid item xs={12} sm={7} md={8} className={classes.topActions}>
            {
              isElectron
                ? <AddFileElectron onFilesChange={this.handleFilesChangeInElectron} />
                : <AddFile onFilesChange={this.handleFilesChangeInHTML} />
            }
            <ImportFile onFileImport={this.handleFileImport} />
          </Grid>
          <Grid item xs={12} sm={5} md={4}>
            <SearchPaper onChange={this.onSearchChange} onFocus={this.onSearchFocus} onUnFocus={this.onSearchUnFocus} />
          </Grid>
        </Grid>
        <CardHeader>
          <RepoSizeCard />
          <PeerCountCard />
          <Hidden smDown>
            <HelpCard tips={[
              "Did you know that you can disable Cards like this one from the settings?",
              "Did you know you can drag and drop files into Orion window?",
              "Siderus' gateway is faster because Orion is directly connected to it",
              "If your friends are not using Orion, you can share a file using the public link",
              "Did you know Orion is developed by Siderus since 2016?",
              "Remember, unencripted files are public and accessible via the IPFS network",
              "On windows you can right click on files and directory to add them to IPFS",
              "Did you know you can host a full website on IPFS using DNSLinks or IPNS?",
              "The equivalent of `ipfs pin add` is `import` when using Orion",
              "Did you know IPFS is developed by a company called Protocol Labs?",
              "Your files are stored only locally until somebody requests them",
              "2 Files with the same content (hash) are not using double spaces",
              <React.Fragment>
                Did you know Orion is developed by{' '}
                <SafeLink href="https://siderus.io">Siderus</SafeLink>{' '}
                since 2016?
              </React.Fragment>,
              <React.Fragment>
                Do you like Orion? <br />
                Follow Siderus on twitter:{' '}
                <SafeLink href="https://twitter.com/IPFSRocks">@IPFSRocks</SafeLink>!
              </React.Fragment>,
              <React.Fragment>
                Do you need help? Check{' '}
                <SafeLink href="https://orion.siderus.io/">orion.siderus.io</SafeLink> for more info!
              </React.Fragment>,
            ]}/>
          </Hidden>
        </CardHeader>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <Paper elevation={4}>
              <ConnectedStorageList isPins list={pagedData}/>
              <TablePagination
                rowsPerPageOptions={[10, 20, 50, 75, 100]}
                component="div"
                count={data.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                  'aria-label': 'Previous Page',
                }}
                nextIconButtonProps={{
                  'aria-label': 'Next Page',
                }}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
              />
            </Paper>
          </Grid>
        </Grid>
      </React.Fragment>
    )
  }
}

/**
 * This function will setup the document and body events to add a file on drag
 * and drop action.
 */
export function setupAddAppOnDrop (callback) {
  document.ondragover = (event) => {
    event.preventDefault()
  }

  document.body.ondrop = (event) => {
    event.preventDefault()
    if (event.dataTransfer) {
      callback(event.dataTransfer.files)
    }
  }
}
