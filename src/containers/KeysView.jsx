import React from 'react'
import { observer, inject } from "mobx-react"
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'
import Hidden from '@material-ui/core/Hidden'
//
import ConnectedKeyList from './ConnectedKeyList'

import { eventTrack } from '../worker/activities'
import { addKey } from '../worker/keys'
import SafeLink from '../components/SafeLink'

import AddKeyButton from '../components/Keys/AddKeyButton'
import ResolveKey from '../components/Keys/ResolveKey'
import Toolbar from '../components/ToolBar/Toolbar'

import HelpCard from '../components/Cards/HelpCard'
import CardHeader from '../components/Cards/CardHeader'
import KeyLifetimeCard from '../components/Cards/KeyLifetimeCard'
import KeyRepublishCard from '../components/Cards/KeyRepublishCard'

const styles = theme => ({
  leftIcon: {
    marginRight: theme.spacing.unit
  }
})

@withStyles(styles)
@inject('KeysStore')
@inject('SettingsStore')
@observer
export default class Names extends React.Component {
  componentDidMount () {
    eventTrack('keys')
    this.props.SettingsStore.fetch()
    .then(() => {
      this.props.KeysStore.startLoop()
    })
  }

  componentWillUnmount () {
    this.props.KeysStore.stopLoop()
  }

  handleKeyAdd = (name) => {
    addKey(name)
      .then(result => {
        console.log('New key added: ', result)
      })
  }

  render () {
    const { KeysStore, SettingsStore } = this.props

    return (
      <React.Fragment>
        <Toolbar>
          <AddKeyButton onNewKey={this.handleKeyAdd} />
          <ResolveKey />
        </Toolbar>
        <CardHeader>
          <KeyLifetimeCard />
          <KeyRepublishCard />
          <Hidden smDown>
            <HelpCard tips={[
              "Did you know that you can disable Cards like this one from the settings?",
              "The equivalent of `ipfs name resolve` is to `Resolve Key` when using Orion App",
              "IPNS Names and Keys are expiring after a while. Open Orion once in a while",
              "Names are an easy way to publish dynamic content using IPNS Keys",
              <React.Fragment>
                Do you like Orion? <br />
                Follow Siderus on twitter:{' '}
                <SafeLink href="https://twitter.com/IPFSRocks">@IPFSRocks</SafeLink>!
              </React.Fragment>,
              <React.Fragment>
                Do you need help? Check{' '}
                <SafeLink href="https://orion.siderus.io/">orion.siderus.io</SafeLink> for more info!
              </React.Fragment>,
            ]}/>
          </Hidden>
        </CardHeader>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <Paper elevation={4}>
              <ConnectedKeyList gateway={SettingsStore.settings.gateway} list={KeysStore.keys} valuesMap={KeysStore.keysResolved} />
            </Paper>
          </Grid>
        </Grid>
      </React.Fragment>
    )
  }
}
