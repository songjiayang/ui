import React from 'react'
import ReactDOM from 'react-dom'
import '../utils/sentry'

// Your top level component
import App from './App'

// Start the web worker
import './worker'

// Export your top level component as JSX (for static rendering)
export default App

/**
 * detect IE
 * returns boolean if the browser is IE, Microsoft Edge or not
 */
function detectIE() {
  const ua = window.navigator.userAgent
  return ua.indexOf('MSIE ') > 0 || ua.indexOf('Trident/') > 0 || ua.indexOf('Edge/') > 0
}

// Render your app
if (typeof document !== 'undefined') {
  const renderMethod = module.hot ? ReactDOM.render : ReactDOM.hydrate || ReactDOM.render
  const render = Comp => {
    renderMethod(<Comp />, document.getElementById('root'))
  }

  // Render!
  render(App)
}

if(detectIE()){
  alert('It looks like you are using a deprecated Browser. You might find ' +
  'issues using Orion. To get the best user experience please use the latest ' +
  'Standalone Application available at https://orion.siderus.io/')
}
