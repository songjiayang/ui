import React from 'react'
import List from '@material-ui/core/List'
import TextSetting from '../../components/Settings/TextSetting'
import BooleanSetting from '../../components/Settings/BooleanSetting'

class Files extends React.Component {
  state = {
    newStorageMax: false,
    newGCPeriod: false
  }

  render () {
    const {
      patchConfig,
      patchSettings,
      skipGatewayQuery,
      disableWrapping,
      Datastore,
      Gateway,
    } = this.props
    const { newStorageMax, newGCPeriod } = this.state

    return (
      <List>
        <TextSetting
          label="Maximum Storage usable by IPFS"
          placeholder="A soft upper limit for the size of the ipfs repository's datastore"
          value={newStorageMax === false ? Datastore.StorageMax : newStorageMax}
          onChange={event => this.setState({ newStorageMax: event.target.value })}
          onSubmit={() => patchConfig('Datastore.StorageMax', newStorageMax)}
        />
        <TextSetting
          label="Garbage Collection period"
          placeholder="A time duration specifying how frequently to run a garbage collection"
          value={newGCPeriod === false ? Datastore.GCPeriod : newGCPeriod}
          onChange={event => this.setState({ newGCPeriod: event.target.value })}
          onSubmit={() => patchConfig('Datastore.GCPeriod', newGCPeriod)}
        />
        <BooleanSetting
          primary="Skip querying gateways after adding a file"
          secondary='If enabled prevent automatically fetching public gateways when adding files. If enabled it will impact content availability online'
          checked={skipGatewayQuery}
          onChange={() => patchSettings({ skipGatewayQuery: !skipGatewayQuery })}
        />
        <BooleanSetting
          primary="Enable wrapping"
          secondary="When adding files wrapping allows to preserve the names"
          checked={!disableWrapping}
          onChange={() => patchSettings({ disableWrapping: !disableWrapping })}
        />
        <BooleanSetting
          primary="Enable Writable Gateway"
          secondary="Allow to use push content via the IPFS Gateway"
          checked={Gateway.Writable}
          onChange={() => patchConfig('Gateway.Writable', !Gateway.Writable)}
        />
      </List >
    )
  }
}

export default Files
