import React from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import ClearIcon from '@material-ui/icons/Clear'
//

const TITLE = 'Activities'
const CLEAR_BUTTON_LABEL = 'Clear'
const TABLE_NAMES = [
  '', // Type e.g. fies/add
  'Name',
  'Progress',
  'Timestamp'
]

const styles = {
  root: {
    overflowX: 'auto'
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  table: {
    tableLayout: 'fixed',
    minWidth: 600,

    '& th:nth-child(1)': { width: 48 },
    '& th:nth-child(2)': { width: '100%' },
    '& th:nth-child(3)': { width: 200 },
    '& th:nth-child(4)': { width: 160 },

    '& td:nth-child(1) svg': {
      // looks to me like the icon is a bit higher than the checkbox
      fontSize: 36,
    },
    '& td:nth-child(2)': {
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },

    '& tbody tr': {
      height: 72
    }
  }
}

const ActivityList = ({ classes, children, onClear, onInvertOrder, sortDirection}) => (
  <div className={classes.root}>
    <Toolbar className={classes.toolbar}>
      <Typography variant='h6'>{TITLE}</Typography>
      <Button onClick={onClear} color='primary'>
        <ClearIcon />
        {CLEAR_BUTTON_LABEL}
      </Button>
    </Toolbar>
    <Table className={classes.table}>
      <TableHead>
        <TableRow>
          <TableCell>{TABLE_NAMES[0]}</TableCell>
          <TableCell>{TABLE_NAMES[1]}</TableCell>
          <TableCell>{TABLE_NAMES[2]}</TableCell>
          <TableCell sortDirection={sortDirection} align='right'>
            <TableSortLabel active
              direction={sortDirection}
              onClick={onInvertOrder}
            >
              {TABLE_NAMES[3]}
            </TableSortLabel>
          </TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {
          children
        }
      </TableBody>
    </Table>
  </div>
)



export default withStyles(styles)(ActivityList)
