import { addBootstrapAddr, connectTo } from "./ipfs/peers"

const USER_AGENT = `SiderusOrion/1.0.0`
const PREFETCH_GATEWAY_FETCH_OPTIONS: RequestInit = {
  cache: "no-cache",
  headers: { "User-Agent": USER_AGENT },
  method: "HEAD",
  mode: "no-cors",
}

const GATEWAYS: string[] = [
  "https://siderus.io/ipfs",
  "https://ipfs.io/ipfs",
  "http://cloudflare-ipfs.com/ipfs",
  "https://gateway.ipfs.io/ipfs",
  "https://xmine128.tk/ipfs",
  "https://gateway.swedneck.xyz/ipfs",
  "https://ipfs.eternum.io/ipfs",
  "https://ipfs.wa.hle.rs/ipfs",
  "https://ipfs.jes.xxx/ipfs",
]

const PEER_LIST_URL = "https://meta.siderus.io/ipfs/peers.txt"

/**
* connectToSiderusNetwork will connect to Siderus Network (Siderus IPFS Peers)
* to ensure faster connections to the gateways
*/
export function connectToSiderusNetwork(): Promise<any> {
  if (!navigator.onLine) {
    return Promise.resolve()
  }

  return getSiderusPeers()
    .then((peers) => Promise.all([
      ...peers.map((addr: string) => connectTo(addr)),
      ...peers.map((addr: string) => addBootstrapAddr(addr)),
    ]))
    .then(() => {
      console.log("[Worker] Connected to Siderus Network")
    })
    .catch((err) => {
      console.error("[Worker] Error while connecting to Siderus Network: ", err)
    })
}

/**
 * getSiderusPeers will fetch the latest peers available and return an array of
 * multiaddress (as strings) of IPFS nodes
 */
export function getSiderusPeers(): Promise<any> {
  if (!navigator.onLine) {
    return Promise.resolve()
  }

  return fetch(PEER_LIST_URL)
    .then((response) => response.text())
    .then((content) => {
      // split the file by endlines
      let peers = content.split(/\r?\n/)
      // remove empty lines
      peers = peers.filter((el) => el.length > 0)
      return Promise.resolve(peers)
    })
}

/**
 * Query the gateways to help content propagation and
 * ensure that the file is available in the network.
 */
export function queryGateways(hash: string): Promise<any> {
  if (!navigator.onLine) {
    return Promise.resolve()
  }

  console.debug("[Worker] Querying gateways for: ", hash)
  return Promise.all(
    GATEWAYS.map((gateway) => {
      fetch(`${gateway}/${hash}`, PREFETCH_GATEWAY_FETCH_OPTIONS)
        .catch((err) => console.warn(`[Worker] Could not query ${gateway}. Error: ${err}`))
    }),
  )
}
