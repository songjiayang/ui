import React from 'react'
import Button from '@material-ui/core/Button'

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

class SafeLinkButton extends React.Component {
  handleOnClick = () => {
    if(isElectron) window.electron.shell.openExternal(this.props.href)
  }

  render() {
    const {children, ...rest} = this.props
    // Disable href and target if it is electron
    rest.href = isElectron ? '' : rest.href
    rest.target = isElectron ? '' : "_blank"

    return (
      <Button onClick={this.handleOnClick} {...rest}>
        {children}
      </Button>
    )
  }
}
export default SafeLinkButton
