import React from 'react'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogContent from '@material-ui/core/DialogContent'
import SafeLink from '../SafeLink'

const PRIVACY_LINK = 'https://siderus.io/privacy'

class TrackingContent extends React.Component {
  render () {
    return (
      <DialogContent>
        <DialogContentText>
          <h2>Support Orion development</h2>
          <p>
            Orion is integrated with Sentry and a custom telemetry
            server to help us debugging errors, understanding <b>how to improve
            the application</b> and your user experience with IPFS.<br />
            Both the integrations are using anonymized information, and
            focusing only on the basic details. Some of them are:
            <ul>
              <li>An unique random ID generated on the first usage</li>
              <li>Features used, Sessions</li>
              <li>Operative System and release/version</li>
              <li>Amount of files shared and the size, NOT the hashes/content</li>
              <li>Relevant information for A/B testing</li>
            </ul>
            You can opt-in as well as opt-out at any time from the Settings.<br />
            By enabling the telemetry, you will provide extra valuable metrics
            and data useful for improving your user experience, the
            services and fixing bugs <b>on top of the default Sentry anonymous
            errors reporting</b> always enabled.<br />
            You can get more information on our webiste or by checking the
            source code on GitLab. You can also read Siderus{' '}
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <SafeLink href={PRIVACY_LINK} target="_blank">
              Privacy policy here
            </SafeLink>.
          </p>
        </DialogContentText>
      </DialogContent>
    )
  }

}

export default TrackingContent
