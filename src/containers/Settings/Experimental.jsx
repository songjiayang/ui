import React from 'react'
import List from '@material-ui/core/List'
import BooleanSeting from '../../components/Settings/BooleanSetting'

class Experimental extends React.Component {
  render () {
    const { Experimental, patchConfig } = this.props

    return (
      <List>
        <BooleanSeting
          primary="Filestore enabled"
          secondary='Allows files to be added without duplicating the space they take up on disk'
          checked={!!Experimental.FilestoreEnabled}
          onChange={() => patchConfig('Experimental.FilestoreEnabled', !Experimental.FilestoreEnabled)}
        />
        <BooleanSeting
          primary="Libp2p Stream Mounting"
          secondary='Allows tunneling of TCP connections through Libp2p streams.'
          checked={!!Experimental.Libp2pStreamMounting}
          onChange={() => patchConfig('Experimental.Libp2pStreamMounting', !Experimental.Libp2pStreamMounting)}
        />
        <BooleanSeting
          primary="Sharding enabled"
          secondary='Allows to create directories with unlimited number of entries'
          checked={!!Experimental.ShardingEnabled}
          onChange={() => patchConfig('Experimental.ShardingEnabled', !Experimental.ShardingEnabled)}
        />
        <BooleanSeting
          primary="Url Store enabled"
          secondary='Allows ipfs to retrieve blocks contents via a url instead of storing it in the datastore'
          checked={!!Experimental.UrlstoreEnabled}
          onChange={() => patchConfig('Experimental.UrlstoreEnabled', !Experimental.UrlstoreEnabled)}
        />
        <BooleanSeting
          primary="P2P HTTP Proxy"
          secondary="Allows proxying of HTTP requests over p2p streams"
          checked={!!Experimental.P2pHttpProxy}
          onChange={() => patchConfig('Experimental.P2pHttpProxy', !Experimental.P2pHttpProxy)}
        />
        <BooleanSeting
          primary="QUIC Protocol"
          secondary='Enables QUIC address, e.g. /ip4/0.0.0.0/udp/4001/quic . Disabled by default'
          checked={!!Experimental.QUIC}
          onChange={() => patchConfig('Experimental.QUIC', !Experimental.QUIC)}
        />
      </List >
    )
  }
}

export default Experimental
