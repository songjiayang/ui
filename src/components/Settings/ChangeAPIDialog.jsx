import React from 'react'

import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { withSnackbar } from 'notistack'
import mafmt from 'mafmt'

import SafeLink from '../SafeLink'
import { patchSettings } from '../../worker/settings'
import { resetIPFSClient } from '../../electron/shell'
import {
  COMPLETED_SETTINGS,
} from '../../notifications'

const CORS_HELP_URL = 'https://stackoverflow.com/questions/42708251/how-to-do-cross-origin-requests-on-ipfs'
const SUCCESS_NOTIFICATION_TEXT = 'Settings updated'
const RELOAD_NOTIFICATION_SETTINGS = { variant: 'warning', autoHideDuration: 3500 }

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

@withSnackbar
export default class ChangeAPIDialog extends React.Component {
  state = {
    newApiAddress: '',
    isValid: true,
  }

  onChange = (e) => {
    const newApiAddress = e.target.value
    let isValid = false
    try {
      isValid = mafmt.TCP.matches(newApiAddress)
    } catch (err) {
      isValid = false
    }

    this.setState({ newApiAddress, isValid })
  }

  resetToLocal = () => {
    this.setState({ newApiAddress: '/ip4/127.0.0.1/tcp/5001' })
  }

  handleSave = () => {
    const { newApiAddress } = this.state
    if(!newApiAddress) return

    console.debug('[SPA] Changing IPFS node to', newApiAddress)
    return patchSettings({ ipfsApiAddress: newApiAddress })
      .then(() => {
        console.debug('[SPA] Updating to', newApiAddress)
        this.props.enqueueSnackbar(SUCCESS_NOTIFICATION_TEXT, COMPLETED_SETTINGS)
        // Let the Shell know we changed the address
        if(isElectron) resetIPFSClient(newApiAddress)
        // The IPFS API address has changed we must refresh the page
        this.props.enqueueSnackbar('IPFS address changed, reloading...', RELOAD_NOTIFICATION_SETTINGS)
        console.debug('[SPA] Reloading')
        setTimeout(()=>{ window.location.reload() }, 2000)
      })
  }

  render() {
    const { newApiAddress, isValid } = this.state
    const { ipfsApiAddress = '', isOpen, handleCloseButtonClick } = this.props

    return (
      <Dialog
      open={isOpen || false}
        onClose={this.handleClose}
        aria-labelledby='form-dialog-title'
      >
        <DialogTitle id='form-dialog-title'>Change IPFS API Multiaddress</DialogTitle>
        <DialogContent>
          <DialogContentText>
            This will allow you to change the current configuration to point to
            a different IPFS API endpoint. The default value is:
            <code role='link' tabIndex='0' onKeyDown={this.resetToLocal} onClick={this.resetToLocal}>
              /ip4/127.0.0.1/tcp/5001
            </code>.
            <br />
            <br />
            Please make sure to enable CORS by following{' '}
            <SafeLink href={CORS_HELP_URL}>these instructions</SafeLink>
          </DialogContentText>
          <TextField
            autoFocus
            margin='dense'
            id='name'
            label='IPFS API multiaddress'
            fullWidth
            value={newApiAddress || ipfsApiAddress}
            onChange={this.onChange}
            error={!isValid}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseButtonClick}>
            Cancel
          </Button>
          <Button onClick={this.handleSave} color='primary' variant='outlined'>
            Save and Reload
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}