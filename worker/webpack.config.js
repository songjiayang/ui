const path = require('path')
const webpack = require('webpack')

module.exports = {
  entry: {
    activities: path.resolve(__dirname, 'src/activities.ts'),
    files: path.resolve(__dirname, 'src/files.ts'),
    keys: path.resolve(__dirname, 'src/keys.ts'),
    settings: path.resolve(__dirname, 'src/settings.ts'),
    connectivity: path.resolve(__dirname, 'src/connectivity.ts'),
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js'
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ]
  },
  node:{
    net: 'empty',
    tls: 'empty',
    dns: 'empty'
  },
  plugins: [
    new webpack.BannerPlugin({
      banner: 'Copyright (c) 2015-2020 Siderus OU - [name] [hash] - [file]'
    })
  ],
	module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: 'ts-loader',
      }
    ]
  },
  optimization: {
    // minimize: true
  },
  performance: {
    hints: false
  }
}