import ipfsAPI from "ipfs-http-client"
import { get as getSettings } from "../settings"

self.ipfs = null

/**
 * getApiClient will provide an existing or a new ipfs-http-client instance
 *
 */
export function getApiClient(): Promise<any> {
  return new Promise<any>((resolve) => {
    if (self.ipfs !== null) { return resolve(self.ipfs) }

    return getSettings()
    .then((settings) => {
      const apiMultiaddress = settings.ipfsApiAddress || "/ip4/127.0.0.1/tcp/5001"
      self.ipfs = ipfsAPI(apiMultiaddress)
      console.log("[Worker] New ipfs connection established to: ", apiMultiaddress)
    })
    .then(() => Promise.resolve(self.ipfs))
  })
}
