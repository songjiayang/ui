import React from 'react'
import { observer, inject } from 'mobx-react'
import WorkIcon from '@material-ui/icons/Work'
import Typography from '@material-ui/core/Typography'

import GenericCard from './GenericCard'

@inject('SettingsStore')
@observer
export default class RepoSizeCard extends React.Component {
  onClick = () => {
    this.props.SettingsStore.fetch()
  }

  render (){
    const { SettingsStore } = this.props
    const { repoSize } = SettingsStore.repoInfo || { value: 0, unit: 'kb'}

    return (
      <GenericCard
        Icon={WorkIcon}
        tooltip={`The size used on disk. This includes files and cached content.`
                + ` To make space run the Garbage Collection from the settings`}
        title="Space used"
        style={{width: 230}}
        onClick={this.onClick}
      >
        <Typography variant="h5" component="h3">
            {repoSize.value || "0"} {repoSize.unit || "kb"}
        </Typography>
      </GenericCard>
    )
  }
}