import React from 'react'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import IconButton from '@material-ui/core/IconButton'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import DeleteIcon from '@material-ui/icons/Delete'
import LinkIcon from '@material-ui/icons/Link'
import EditIcon from '@material-ui/icons/Edit'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Checkbox from '@material-ui/core/Checkbox'
import { withStyles } from '@material-ui/core/styles'
import Divider from '@material-ui/core/Divider'

import OpenInBrowser from '../MenuElements/OpenInBrowser'
import CopyButton from '../MenuElements/Copy'
//

const ACTION_MENU_ITEMS = [
  // The handlers are specifid in ConnectedKeyList
  { handlerName: 'onUpdate', label: 'Update', icon: <LinkIcon /> },
  { handlerName: 'onRename', label: 'Rename', icon: <EditIcon /> },
  { handlerName: 'onDelete', label: 'Remove', icon: <DeleteIcon /> },
]

class KeyElement extends React.Component {
  state = {
    actionMenuAnchor: null
  }

  handleActionMenuOpen = (event) => {
    this.setState({ actionMenuAnchor: event.currentTarget })
  }

  handleActionMenuClose = () => {
    this.setState({ actionMenuAnchor: null })
  }

  render () {
    const { actionMenuAnchor } = this.state
    const {
      classes, gateway,
      id, name, value,
      isSelected, onSelect
    } = this.props

    const ClickableCell = props => (
      <TableCell
        className={classes.clickableCell}
        onClick={() => { onSelect(id) }}
        {...props}
      />
    )

    return (
      <TableRow
        hover
        role='checkbox'
        aria-checked={isSelected}
        selected={isSelected}
        forkeyid={id}
      >
        <ClickableCell>
          <Checkbox checked={isSelected} />
        </ClickableCell>
        <ClickableCell component='th' scope='row'>
          {id}
        </ClickableCell>
        <ClickableCell>
          {name}
        </ClickableCell>
        <ClickableCell>
          {value || 'Unknown'}
        </ClickableCell>
        <TableCell align='right'>
          <IconButton
            aria-label='Actions'
            aria-owns={actionMenuAnchor ? 'action-menu' : null}
            aria-haspopup='true'
            onClick={this.handleActionMenuOpen}
          >
            <MoreHorizIcon />
          </IconButton>
          <Menu
            id='action-menu'
            anchorEl={actionMenuAnchor}
            open={Boolean(actionMenuAnchor)}
            onClose={this.handleActionMenuClose}
          >
            <OpenInBrowser key={0} to={`${gateway}/ipns/${id}`} />
            <CopyButton key={1} value={id} />
            <Divider key={2} />
            {
              ACTION_MENU_ITEMS.map(({ label, icon, handlerName, hidden }, index) => {
                if (hidden && hidden())
                  return null

                if (label === null)
                  return <Divider key={index+3} />

                return (
                  <MenuItem key={index+3} handler={handlerName}>
                    <ListItemIcon>
                      {icon}
                    </ListItemIcon>
                    <ListItemText
                      inset
                      primary={label}
                      onClick={() => {
                        this.handleActionMenuClose()
                        const handler = this.props[handlerName]
                        handler(id)
                      }}
                    />
                  </MenuItem>
                )
              })
            }
          </Menu>
        </TableCell>
      </TableRow>
    )
  }

}

const styles = {
  clickableCell: {
    cursor: 'pointer'
  }
}

export default withStyles(styles)(KeyElement)
