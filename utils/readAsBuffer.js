/**
 *
 * FileWithContent {
 *   name: string
 *   content: Buffer
 * }
 *
 * Warning: this does not work with directories
 *
 * @param {File} file
 * @returns {FileWithContent}
 */
export function readAsBuffer (file) {
  return new Promise((resolve, reject) => {
    const reader = new window.FileReader()
    reader.onload = () => {
      resolve({
        content: Buffer.from(reader.result),
        name: file.name
      })
    }
    // If you try to read a directory (e.g. with drag and drop), this will fail
    reader.onerror = () => {
      reject(new Error(reader.error))
    }

    reader.readAsArrayBuffer(file)
  })
}
