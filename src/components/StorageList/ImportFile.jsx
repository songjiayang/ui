import React from 'react'
import SaveAlt from '@material-ui/icons/SaveAlt'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import LinearProgress from '@material-ui/core/LinearProgress'
import Typography from '@material-ui/core/Typography'

// FIXME https://github.com/nozzle/react-static/issues/816
import { cid as isCID } from 'is-ipfs'
import { withRouter } from 'react-router'

import {
  // getFilePeers,
  getFileStat,
} from '../../worker/files'

const IMPORT_BUTTON_LABEL = 'Import files'

// const isCID = hash => typeof hash === 'string' && hash.length === 46

const styles = theme => ({
  content: {
    minWidth: 500,
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  }
})


@withRouter
@withStyles(styles)
class ImportFile extends React.Component {
  initialState = {
    open: false,
    hash: '',
    isValid: true,
    stat: null,
    peers: null,

    lastHandledRequestId: -1
  }

  state = this.initialState

  handleOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({
      open: false,
      hash: '',
      isValid: true,
      stat: null,
      peers: null
    })
  }

  handleSubmit = () => {
    const { onFileImport } = this.props
    const { hash, isValid } = this.state

    if (hash && isValid) {
      onFileImport(hash)
      this.handleClose()
    }
  }

  static getDerivedStateFromProps (props, state) {
    if (props.location.state) {
      // if this was a redirect containing a state with request, handle it
      const { request } = props.location.state

      // only handle this request if the type is open-dialog and we haven't reacted on it before
      if (request && request.type === 'open-dialog' && request.id > state.lastHandledRequestId) {
        return { ...state, open: true, lastHandledRequestId: request.id }
      }
    }

    return state
  }

  onInputChange = (event) => {
    const hash = event.target.value
    const isValid = isCID(hash)

    this.setState({ hash, isValid, stat: null, peers: null })

    if (isValid) {
      // getFilePeers(hash).then(peers => this.setState({ peers }))
      getFileStat(hash).then(stat => this.setState({ stat }))
    }
  }

  render () {
    const { classes } = this.props
    const { open, hash, isValid, stat } = this.state

    return (
      <React.Fragment>
        <Button onClick={this.handleOpen}>
          <SaveAlt className={classes.leftIcon} />
          {IMPORT_BUTTON_LABEL}
        </Button>
        <Dialog
          open={open}
          onClose={this.handleClose}
        >
          {
            isValid && hash && (stat === null) &&
            <LinearProgress variant='query' />
          }
          <DialogTitle>Import from hash</DialogTitle>
          <DialogContent className={classes.content}>
            <DialogContentText>
              Insert the hash you would like to add to your peer:
            </DialogContentText>
            <TextField
              error={!isValid}
              value={hash}
              onChange={this.onInputChange}
              autoFocus
              fullWidth
              placeholder='Qma1B2C4d4...'
            />
            {
              // isValid && hash &&
              // <Typography>
              //   {'Peers with this Object: '}
              //   <b>{peers ? peers.length : 'Loading...'}</b>
              // </Typography>
            }
            {
              isValid && hash &&
              <Typography>
                {'Object cumulative size: '}
                <b>{stat ? `${stat.CumulativeSize.value} ${stat.CumulativeSize.unit}` : 'Loading...'}</b>
              </Typography>
            }
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose}>
              Cancel
            </Button>
            <Button
              onClick={this.handleSubmit}
              color='primary'
              variant='outlined'
              disabled={!isValid}
            >
              Import
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    )
  }
}


export default ImportFile