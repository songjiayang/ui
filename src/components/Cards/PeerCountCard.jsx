import React from 'react'
import { observer, inject } from 'mobx-react'
import PeopleIcon from '@material-ui/icons/People'
import Typography from '@material-ui/core/Typography'

import GenericCard from './GenericCard'

@inject('NetworkStore')
@observer
export default class PeerCountCard extends React.Component {
  onClick = () => {
    this.props.NetworkStore.fetch()
  }

  render (){
    const { NetworkStore } = this.props
    return (
      <GenericCard
        Icon={PeopleIcon}
        tooltip="The amount of IPFS Peers we are connected with"
        title="Peers connected"
        style={{width: 230}}
      >
        { NetworkStore.peers &&
          <Typography variant="h5" component="h3">
              {NetworkStore.peers.length || "0"}
          </Typography>
        }
      </GenericCard>
    )
  }
}