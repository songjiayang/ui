/// <reference path="../../worker/src/definitions/object.d.ts" />
/// <reference path="../../worker/src/definitions/stats.d.ts" />
/// <reference path="../../worker/src/definitions/api.d.ts" />

import { action, observable } from "mobx"
import { getPinList, importFile, removeFiles } from "../worker/files"

class FilesStore {
  @observable public loading = true
  @observable public pins: EnhancedObject[] = []
  private fetching = false
  private interval = null

  @action
  public fetch = () => {
    this.fetching = true
    return getPinList().then((pins) => {
      this.pins = pins
      this.loading = false
      this.fetching = false
      return Promise.resolve()
    })
  }

  @action
  public startLoop = (frequency = 1500) => {
    if (this.interval) {
      clearInterval(this.interval)
    }

    this.interval = setInterval(() => {
      if (this.fetching) {
        return
      }

      this.fetch()
    }, frequency)
  }

  @action
  public stopLoop = () => {
    clearInterval(this.interval)
  }

  public search = (term: string): any[] => {
    return this.pins.filter((pin) => {
      return pin.hash.indexOf(term) >= 0 ||
        pin.description.indexOf(term) >= 0 ||
        pin.dag.links.find((el) => el.path.indexOf(term) >= 0 || el.hash.indexOf(term) >= 0)
    })
  }

  public isPinned = (hash: string): Promise<boolean> => {
    return new Promise((resolve) => {
      if (!hash) {
        return resolve(false)
      }

      return getPinList(hash)
        .then((res) => resolve(!!res))
        .catch(() => resolve(false))
    })
  }

  public import = (hash: string): Promise<any> => {
    return importFile(hash).then( (anything) => {
      this.fetch()
      return Promise.resolve(anything)
    })
  }

  public remove = (hash: string | string[]): Promise<any> => {
    return removeFiles(hash).then( (anything) => {
      this.fetch()
      return Promise.resolve(anything)
    })
  }
}

const store = new FilesStore()
export default store
