const WAIT_FOR_WEBWORKER_DELAY = 1 * 1000
const WAIT_FOR_WEBWORKER_INITIAL_DELAY = 2.5 * 1000

const WEB_WORKER_TIMEOUT_ERROR = "Requested timeout"
const WEB_WORKER_TIMEOUT_TIME = 30 * 1000
const WEB_WORKER_PROMISES_CLEANUP = 10 * 1000 // 10 seconds to cleanup res, rej

function genWebWorkerTimeoutErrorWithMethod(method: string) {
  return Error(`${WEB_WORKER_TIMEOUT_ERROR} ${method}`)
}

export function promiseTimeout(promise) {
  // Create a promise that rejects in <ms> milliseconds
  const timeout = new Promise((resolve, reject) => {
    const id = setTimeout(() => {
      clearTimeout(id)
      reject(WEB_WORKER_TIMEOUT_ERROR)
    }, WEB_WORKER_TIMEOUT_TIME)
  })

  // Returns a race between our timeout and the passed in promise
  return Promise.race([
    promise,
    timeout,
  ])
}

// SUBSCRIPTIONS will map all the callback available with a specific method
// {'method': callback }
const resolves = {}
const rejects = {}
let globalMsgId: number = 0

// Cleans the promises objects from memory
function cleanMsgPromise(id: number) {
  setTimeout(() => {
    delete resolves[id]
    delete rejects[id]
  }, WEB_WORKER_PROMISES_CLEANUP)
}

// What do we do when we receive a message?
export function onWorkerMessage(msg: {data: EventMessage}) {
  const {id, error, value} = msg.data

  // remove the promsie resolve/rejects after WEB_WORKER_PROMISES_CLEANUP
  // this is done only if we receive a message from the worker.
  cleanMsgPromise(id)

  // in case of an error, report it
  if (error) {
    const reject = rejects[id]
    if (reject) { reject(error) }

    return
  }

  // in case of success, resolve the promise
  const resolve = resolves[id]
  if (resolve) {
    if (value) { return resolve(value) }
    return resolve()
  }
  return
}

/**
 * Send a message to the web worker and wait for the response.
 * It will create a new subscription for that method and wait for the result
 * using promises.
 * ToDo: Support multiple subscriptions for same method
 *
 */
export function requestWorker(worker: Worker, request: EventRequest, timeout = WEB_WORKER_TIMEOUT_TIME): Promise<any> {

  return new Promise((resolve, reject) => {
    request.id = globalMsgId++
    resolves[request.id] = resolve
    rejects[request.id] = reject

    // send message
    worker.postMessage(request)

    if (timeout <= 0) { return }

    // Set a Timeout error in case we have not heard any response
    setTimeout(() => {
      // if the resolve and reject have been already executed skip this
      if (resolves[request.id] === undefined || rejects[request.id] === undefined) {
        return
      }

      // Run then Reject and clean the promise after 10 sec from memory
      const customError = genWebWorkerTimeoutErrorWithMethod(request.method)
      reject(customError)
      cleanMsgPromise(request.id)
    }, timeout)
  })
}

// Send a message to
export function sendWorker(worker: Worker, request: any) {
  worker.postMessage(request)
  return Promise.resolve()
}

// Request a simple PING
export function pingWorker(worker: Worker) {
  return requestWorker(worker, { method: "ping" }, 1000)
}

/*
 * Alive check
 */
export function isWorkerAlive(worker: Worker) {
  return pingWorker(worker).then((result) => result === "pong").catch(() => false)
}

// Please call this method only when the app starts
export function waitForWorker(worker: Worker, name) {
  let intervallID
  let trials = 0
  console.debug(`[SPA] Waiting for ${name} worker`)

  return new Promise((resolve, reject) => {
    // adds a WAIT_FOR_WEBWORKER_DELAY*3 to ensure that the worker is started
    setTimeout(() => {
      intervallID = setInterval(() => {
        isWorkerAlive(worker)
        .then((result) => {
          if (!result) {
            console.debug(`[SPA] Still waiting for ${name} worker`)
            trials += 1
            if (trials >= 60 ) {
              console.error(`[SPA] Unable to start ${name}. Reloading page`)
              return reject(`Unable to start ${name} worker`)
            }
            return
          }
          // if the promise returned it is alive
          clearInterval(intervallID)
          resolve()
        }).catch(reject)
      }, WAIT_FOR_WEBWORKER_DELAY)
    }, WAIT_FOR_WEBWORKER_INITIAL_DELAY)
  })
}
