import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import PlayCircleOutline from '@material-ui/icons/PlayCircleOutline'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogContent from '@material-ui/core/DialogContent'
import IPFSLogo from '../../assets/ipfs-logo.svg'
import SafeLinkButton from '../SafeLinkButton'

const VIDEO_LINK = 'https://www.youtube.com/watch?v=5Uj6uR3fp-U&utm_source=siderus-orion&ref=siderus-orion'
const IPFS_WEBSITE = 'https://ipfs.io'

class DescriptionContent extends React.Component {
  render () {
    const { classes } = this.props

    return (
      <DialogContent className={classes.content}>
        <div className={classes.centered}>
          <img src={IPFSLogo} alt='IPFS logo' width='75px' height='75px'/>
        </div>
        <DialogContentText>
          <h2 className={classes.centered}>What is IPFS?</h2>
          <p>
            The InterPlanetary File System (IPFS) is a peer-to-peer hypermedia
            protocol to make the web faster, safer and distributed. IPFS is
            similar to the World Wide Web, Bittorrent and Git.
          </p>
          <h3>What can I do with Orion and IPFS?</h3>
          <p>
            <ul>
              <li>Share public files, images, videos, forever, no censorship</li>
              <li>Publish your own decentralised website</li>
              <li>Store content from your dApp with any Blockchain</li>
            </ul>
          </p>
          <div className={classes.centered}>
            <SafeLinkButton
              color="primary"
              variant="contained"
              className={classes.mainButton}
              href={VIDEO_LINK}
            >
              <PlayCircleOutline /> &nbsp; Play the video
            </SafeLinkButton><br />
            <SafeLinkButton
              color="primary"
              variant="outlined"
              href={IPFS_WEBSITE}
            >
              Visit IPFS.io website
            </SafeLinkButton>
          </div>
        </DialogContentText>
      </DialogContent>
    )
  }
}

const styles = {
  centered: {
    align: 'center',
    textAlign: 'center',
  },
  content: {},
  mainButton: {
    color: 'white',
    marginBottom: 5,
  },
}

export default withStyles(styles)(DescriptionContent)
