interface PeerId {
  id: Multihash
  publicKey: string
  addresses: any[]
  agentVersion: string
  protocolVersion: string
}

interface KnownPeer {
  id: Multihash
  addresses: string[]
  name?: string
  status?: string
}

interface SimplePeer {
  address: Multiaddress,
  id: Multihash,
  location?: any,
}
