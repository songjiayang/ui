import "../../utils/sentry"
import { getApiClient } from "./lib/ipfs/api"
import {
  getKeys, newKey, publishName, removeKey, renameKey, resolveKeyValues,
} from "./lib/ipfs/keys"

console.log("[Pins Worker] Starting")

// Load node modules such as path,fs,etc. under `self.node`
self.importScripts("load-node-modules-into-worker.js")

if (!navigator.onLine) {
  console.warn("[Pins Worker] No internet connection detected")
}

onmessage = (event: MessageEvent) => {
  const request: EventMessage = event.data
  // console.log("[Pins Worker] Request from client:", request)

  const reply = (responseValue: any) => {
    const message: EventMessage = {
      id: request.id,
      method: request.method,
      value: responseValue,
    }
    // console.log("[Pins Worker] Response to client:", request.method, responseValue)
    postMessage(message)
  }

  const replyWithError = (error: Error) => {
    console.error("[Pins Worker] Error during request:", request.method, error.message)
    const message: EventMessage = {
      error: error.message,
      id: request.id,
      method: request.method,
    }
    postMessage(message)
  }

  switch (request.method) {
    case "ping": {
      return getApiClient().then(() => reply("pong")).catch(replyWithError)
    }
    /**
     * Keys
     */
    case "keys/ls": {
      return getKeys().then(reply).catch(replyWithError)
    }
    case "keys/add": {
      return newKey(request.value).then(reply).catch(replyWithError)
    }
    case "keys/rename": {
      return renameKey(request.value.oldName, request.value.newName).then(reply).catch(replyWithError)
    }
    case "keys/rm": {
      return removeKey(request.value).then(reply).catch(replyWithError)
    }
    case "keys/values": {
      return resolveKeyValues(request.value).then(reply).catch(replyWithError)
    }
    case "ipns/publish": {
      return publishName(request.value.name, request.value.value).then(reply).catch(replyWithError)
    }

    default: {
      return replyWithError(new Error(`Method not supported: ${request.method}`))
    }
  }
}

console.log("[Pins Worker] Ready")
