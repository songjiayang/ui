import * as Sentry from '@sentry/browser'
import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import Paper from '@material-ui/core/Paper'
import Popper from '@material-ui/core/Popper'
import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import { resetConfiguration, clearCache } from '../worker/settings'
import { eventTrack } from '../worker/activities'
import OpenInBrowser from './MenuElements/OpenInBrowser'
import ChangeAPIDialog from './Settings/ChangeAPIDialog'

const CORS_HELP_URL = 'https://stackoverflow.com/questions/42708251/how-to-do-cross-origin-requests-on-ipfs'

const styles = {
  dialog: {
    'z-index': 998
  }
}


@withStyles(styles)
class ErrorDialog extends React.Component {
  state = {
    solMenuOpen: false,
    showDialogChangeAPI: false,
  }

  componentDidMount() {
    eventTrack('error', {error: this.props.error.toString()})
  }

  toggleSolutionMenu = () => {
    this.setState({solMenuOpen: !this.state.solMenuOpen})
  }

  toggleChangeIPFSAPIDialog = () => {
    this.setState({ showDialogChangeAPI: !this.state.showDialogChangeAPI })
  }

  onResetConfiguration = () => {
    eventTrack('error/resetConfig')
    console.log('[Error Dialog] Resetting the configuration...')
    resetConfiguration().then(() => {
      window.location = window.location.origin + window.location.pathname
    })
  }

  onClearCache = () => {
    eventTrack('error/clearCache')
    console.log('[Error Dialog] Clearing the app cache...')
    clearCache().then(() => {
      window.location = window.location.origin + window.location.pathname
    })
  }

  onReloadClick = () => {
    eventTrack('error/reload')
    document.location.reload()
  }

  onShowReportDialog = () => {
    eventTrack('error/report')
    const eventId = Sentry.captureException(this.props.error)
    Sentry.showReportDialog({
      eventId,
      successMessage: "Your feedback has been sent. Thank you! You can now reload the page or restart Orion"
    })
  }

  render () {
    const { error, classes } = this.props
    const { solMenuOpen, showDialogChangeAPI } = this.state

    return (
      <React.Fragment>
        <Dialog
          open
          className={classes.dialog}
          disableEscapeKeyDown
          disableBackdropClick
        >
          <DialogTitle>Oops an Error happened</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Something went wrong and an error occurred. Most of the times this
              occurs because of a wrong connection to the IPFS network,
              you can try to solve this using the Quick Fix button<br />
              <br/>
              If the error persist, please report it using the button.
              The error reported: <br/>
              <code>{error.toString()}</code>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button buttonRef={node => {
                this.anchorEl = node
              }}
              aria-owns={solMenuOpen ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={this.toggleSolutionMenu}
            >Quick Fix
            </Button>
            <Popper open={solMenuOpen} anchorEl={this.anchorEl} disablePortal>
              <Paper>
                <ClickAwayListener onClickAway={this.toggleSolutionMenu}>
                  <MenuList>
                    <OpenInBrowser label="How to fix CORS errors" disableIcon to={CORS_HELP_URL}/>
                    <MenuItem onClick={this.toggleChangeIPFSAPIDialog}>Change IPFS API</MenuItem>
                    <MenuItem onClick={this.onResetConfiguration}>Reset Configuration</MenuItem>
                    <MenuItem onClick={this.onClearCache}>Clear App Cache</MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Popper>

            <Button onClick={this.onShowReportDialog}>Report the error</Button>
            <Button onClick={this.onReloadClick} variant='outlined' color='primary'>Try Again</Button>
          </DialogActions>
        </Dialog>
        <ChangeAPIDialog
          isOpen={showDialogChangeAPI}
          handleCloseButtonClick={this.handleChangeAPIToggle} />
      </React.Fragment>
    )
  }
}

export default ErrorDialog
