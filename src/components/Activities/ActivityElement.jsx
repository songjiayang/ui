import React from 'react'
import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'
import moment from 'moment'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import LinearProgress from '@material-ui/core/LinearProgress'


const styles = () => ({
  clickable: {
    cursor: 'pointer'
  },
})

@withStyles(styles)
@withRouter
class ActivityElement extends React.Component {
  handleOnClick = () => {
    const { link, history } = this.props
    if (link) {
      history.push(link)
    }
  }

  render () {
    const { type, name, timestamp, status, progress, link, classes } = this.props
      return (
        <TableRow hover onClick={this.handleOnClick} className={link ? classes.clickable : null} >
          <TableCell>
            {type}
          </TableCell>
          <TableCell component="th" scope="row">
            {name}
          </TableCell>
          <TableCell>
            {
              status !== "in progress"
                ? status
                : <LinearProgress value={progress * 100} color="secondary" variant={progress > 0 ? "determinate" : "query"}/>
            }
          </TableCell>
          <TableCell align='right'>
            {moment(timestamp).fromNow()}
          </TableCell>
        </TableRow>
      )
  }
}

export default ActivityElement
